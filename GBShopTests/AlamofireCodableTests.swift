//
//  AlamofireCodableTests.swift
//  GBShopTests
//
//  Created by Тигран on 24/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
import Alamofire
import OHHTTPStubs
@testable import GBShop

final class AlamofireCodableTests: XCTestCase {
	
	var errorParser: ErrorParserMock!
	var sessionManager: SessionManager!
	
	override func setUp() {
		errorParser = ErrorParserMock()
		let config = URLSessionConfiguration.ephemeral
		OHHTTPStubs.setEnabled(true, for: config)
		sessionManager = SessionManager(configuration: config)
		
		super.setUp()
	}
	
	override func tearDown() {
		removeAllHTTPStubs()
		
		super.tearDown()
	}
	
	func testShouldParse() {
		let expectation = self.expectation(description: "Parse data succeed")
		stubHTTPResponse(method: .get, fileName: "loginStub", path: "/login", statusCode: 200)
		var error: Error?
		
		sessionManager
			.request("http://0.0.0.0:8181/login")
			.responseCodable(errorParser: errorParser) { (response: DataResponse<LoginResultStub>) in
			error = response.result.error
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: 5)
		XCTAssertNil(error)
	}
	
	func testShouldNotParse() {
		let expectation = self.expectation(description: "Parse data failed")
		stubHTTPResponse(method: .get, fileName: "wrongLoginStub", path: "/login", statusCode: 200)
		var error: Error?
		
		sessionManager
			.request("http://0.0.0.0:8181/login")
			.responseCodable(errorParser: errorParser) { (response: DataResponse<LoginResultStub>) in
			error = response.result.error
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: 5)
		XCTAssertNotNil(error)
	}
	
	func testShouldFail() {
		let expectation = self.expectation(description: "Parse response with error succeed")
		stubHTTPResponse(method: .get, fileName: "loginStub", path: "/login", statusCode: 200)
		var error: Error?
		errorParser.error = AppError.undefinedError("Some error")
		
		sessionManager
			.request("http://0.0.0.0:8181/login")
			.responseCodable(errorParser: errorParser) { (response: DataResponse<LoginResultStub>) in
			error = response.result.error
			expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: 5)
		XCTAssertNotNil(error)
	}
	
	func testShouldGetFailResult() {
		let expectation = self.expectation(description: "Response with .failure result")
		stubHTTPResponse(method: .get, fileName: "", path: "/login", statusCode: 500)
		var error: Error?
		errorParser.error = AppError.undefinedError("Some error")
		
		sessionManager
			.request("http://0.0.0.0:8181/login")
			.responseCodable(errorParser: errorParser) { (response: DataResponse<LoginResultStub>) in
				error = response.result.error
				expectation.fulfill()
		}
		
		wait(for: [expectation], timeout: 5)
		XCTAssertNotNil(error)
	}
	
}
