//
//  ErrorParserTests.swift
//  GBShopTests
//
//  Created by Тигран on 01/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
@testable import GBShop

final class ErrorParserTests: XCTestCase {

	var error: Error?
	var nserror: NSError?
	var errorParser: ErrorParser!
	
    override func setUp() {
		errorParser = ErrorParser()
		
		super.setUp()
    }

    override func tearDown() {
		super.tearDown()
    }
	
	func testShouldParseDecodingError() {
		error = DecodingError
			.keyNotFound(CodingKeyStub(stringValue: "user_id")!,
						 DecodingError.Context(codingPath: [], debugDescription: ""))
		let parseError = errorParser.parse(error!)
		XCTAssert(parseError.description == error!.localizedDescription)
	}
	
	func testShouldParseNetworkError() {
		nserror = NSError(domain: "NetworkError", code: -1001, userInfo: nil)
		let parseError = errorParser.parse(nserror!)
		XCTAssert(parseError.description == nserror!.domain)
	}
	
	func testShouldParseClientError() {
		nserror = NSError(domain: "ClientError", code: 403, userInfo: nil)
		let parseError = errorParser.parse(nserror!)
		XCTAssert(parseError.description == nserror!.domain)
	}
	
	func testShouldParseServerError() {
		nserror = NSError(domain: "ServerError", code: 500, userInfo: nil)
		let parseError = errorParser.parse(nserror!)
		XCTAssert(parseError.description == nserror!.domain)
	}
	
	func testShouldParseUndefinedError() {
		error = NSError(domain: "UndefinedError", code: 300, userInfo: nil)
		let parseError = errorParser.parse(error!)
		XCTAssert(parseError.description == error!.localizedDescription)
	}
	
	func testShouldParseResponseUndefinedError() {
		error = NSError(domain: "", code: 999, userInfo: nil)
		let parseError = errorParser.parse(response: nil, data: nil, error: error)
		XCTAssert(parseError!.description == error!.localizedDescription)
	}
	
	func testShouldParseResponseNilError() {
		let parseError = errorParser.parse(response: nil, data: nil, error: nil)
		XCTAssert(parseError == nil)
	}

}
