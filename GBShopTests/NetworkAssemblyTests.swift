//
//  NetworkAssemblyTests.swift
//  GBShopTests
//
//  Created by Тигран on 02/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
@testable import GBShop

final class NetworkAssemblyTests: XCTestCase {

	func testShouldReturnAuthFactory() {
		let authFactory = di.resolve(AuthRequestFactory.self)
		XCTAssertNotNil(authFactory)
	}

	func testShouldReturnChangeProfileFactory() {
		let changeFactory = di.resolve(ChangeProfileRequestFactory.self)
		XCTAssertNotNil(changeFactory)
	}
	
	func testShouldReturnCatalogFactory() {
		let catalogFactory = di.resolve(CatalogRequestFactory.self)
		XCTAssertNotNil(catalogFactory)
	}
	
	func testShouldReturnReviewFactory() {
		let reviewFactory = di.resolve(ReviewRequestFactory.self)
		XCTAssertNotNil(reviewFactory)
	}
	
	func testShouldReturnBasketFactory() {
		let basketFactory = di.resolve(BasketRequestFactory.self)
		XCTAssertNotNil(basketFactory)
	}

}
