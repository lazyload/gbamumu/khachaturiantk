//
//  UserProtocolTests.swift
//  GBShopTests
//
//  Created by Тигран on 26/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
@testable import GBShop

final class UserProtocolTests: XCTestCase {

	func testShouldCreateUser() {
		let user = User(userId: 123,
						username: "username",
						password: "password",
						email: "email")
		XCTAssertEqual(user.userId, 123)
		XCTAssertEqual(user.username, "username")
		XCTAssertEqual(user.password, "password")
		XCTAssertEqual(user.email, "email")
	}

}
