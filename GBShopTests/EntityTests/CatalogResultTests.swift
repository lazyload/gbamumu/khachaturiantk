//
//  CatalogResultTests.swift
//  GBShopTests
//
//  Created by Тигран on 03/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
@testable import GBShop

class CatalogResultTests: XCTestCase {
	
	func testAllFieldsParse() {
		let object = """
			[
				{
					"id_product": 123,
					"product_name": "Ноутбук",
					"price": 4560000
				},
				{
					"id_product": 456,
					"product_name": "Мышка",
					"price": 100000
				}
			]
		"""
		let data = object.data(using: .utf8)!
		
		do {
			let results = try JSONDecoder().decode([CatalogResult].self, from: data)
			XCTAssertEqual(results[0].id, 123)
			XCTAssertEqual(results[0].name, "Ноутбук")
			XCTAssertEqual(results[0].price, 4560000)
			XCTAssertEqual(results[1].id, 456)
			XCTAssertEqual(results[1].name, "Мышка")
			XCTAssertEqual(results[1].price, 100000)
		} catch {
			XCTFail(error.localizedDescription)
		}
	}
	
}
