//
//  UserTests.swift
//  GBShopTests
//
//  Created by Тигран on 03/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
@testable import GBShop

class UserTests: XCTestCase {

	func testAllFieldsParse() {
		let object = """
			{
				"id_user": 1,
				"user_login": "username",
				"user_name": "Tigran",
				"user_lastname": "Khachaturian",
				"email": "123@mail.ru",
				"is_admin": true,
				"gender": "m",
				"credit_card": "1234567890123456",
				"bio": "Что-то обо мне!"
			}
		"""
		let data = object.data(using: .utf8)!
		
		do {
		let user = try JSONDecoder().decode(UserResult.self, from: data)
			XCTAssertEqual(user.id, 1)
			XCTAssertEqual(user.login, "username")
			XCTAssertEqual(user.firstname, "Tigran")
			XCTAssertEqual(user.lastname, "Khachaturian")
			XCTAssertEqual(user.email, "123@mail.ru")
			XCTAssertEqual(user.isAdmin, true)
			XCTAssertEqual(user.gender, "m")
			XCTAssertEqual(user.creditCard, "1234567890123456")
			XCTAssertEqual(user.bio, "Что-то обо мне!")
		} catch {
			XCTFail(error.localizedDescription)
		}
	}
	
}
