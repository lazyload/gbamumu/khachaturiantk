//
//  BasketResultTests.swift
//  GBShopTests
//
//  Created by Тигран on 14/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
@testable import GBShop

class BasketResultTests: XCTestCase {
	
	func testAllFieldsParse() {
		let object = """
			{
		  		"amount": 46600,
		  		"countGoods": 2,
		  		"contents": [
		  		  {
		  		    "id_product": 123,
		  		    "product_name": "Ноутбук",
		  		    "price": 4560000,
					"quantity": 1
		  		  },
				  {
		      		"id_product": 456,
		      		"product_name": "Мышка",
		      		"price": 100000,
		      		"quantity": 1
				  }
				]
			}
		"""
		let data = object.data(using: .utf8)!
		
		do {
			let result = try JSONDecoder().decode(BasketResult.self, from: data)
			XCTAssertEqual(result.amount, 46600)
			XCTAssertEqual(result.countGoods, 2)
			XCTAssertEqual(result.contents?.count, 2)
			XCTAssertEqual(result.contents?[0].id, 123)
			XCTAssertEqual(result.contents?[0].name, "Ноутбук")
			XCTAssertEqual(result.contents?[0].price, 4560000)
			XCTAssertEqual(result.contents?[0].quantity, 1)
			XCTAssertEqual(result.contents?[1].id, 456)
			XCTAssertEqual(result.contents?[1].name, "Мышка")
			XCTAssertEqual(result.contents?[1].price, 100000)
			XCTAssertEqual(result.contents?[1].quantity, 1)
		} catch {
			XCTFail(error.localizedDescription)
		}
	}
	
}
