//
//  ProductResultTests.swift
//  GBShopTests
//
//  Created by Тигран on 03/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
@testable import GBShop

class ProductResultTests: XCTestCase {
	
	func testAllFieldsParse() {
		let object = """
			{
				"result": 1,
				"product_name": "Ноутбук",
				"product_price": 4560000,
				"product_description": "Мощный игровой ноутбук"
			}
		"""
		let data = object.data(using: .utf8)!
		
		do {
			let result = try JSONDecoder().decode(ProductResult.self, from: data)
			XCTAssertEqual(result.result, 1)
			XCTAssertEqual(result.name, "Ноутбук")
			XCTAssertEqual(result.price, 4560000)
			XCTAssertEqual(result.description, "Мощный игровой ноутбук")
		} catch {
			XCTFail(error.localizedDescription)
		}
	}
	
}
