//
//  MessageResultTests.swift
//  GBShopTests
//
//  Created by Тигран on 03/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
@testable import GBShop

class MessageResultTests: XCTestCase {
	
	func testAllFieldsParse() {
		let object = """
			{
				"result" : 1,
				"userMessage" : "Регистрация прошла успешно!"
			}
		"""
		let data = object.data(using: .utf8)!
		
		do {
			let result = try JSONDecoder().decode(MessageResult.self, from: data)
			XCTAssertEqual(result.result, 1)
			XCTAssertEqual(result.message, "Регистрация прошла успешно!")
		} catch {
			XCTFail(error.localizedDescription)
		}
	}
	
}
