//
//  SimpleResultTests.swift
//  GBShopTests
//
//  Created by Тигран on 03/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
@testable import GBShop

class SimpleResultTests: XCTestCase {
	
	func testAllFieldsParse() {
		let object = """
			{ "result" : 1 }
		"""
		let data = object.data(using: .utf8)!
		
		do {
			let result = try JSONDecoder().decode(SimpleResult.self, from: data)
			XCTAssertEqual(result.result, 1)
		} catch {
			XCTFail(error.localizedDescription)
		}
	}
	
}
