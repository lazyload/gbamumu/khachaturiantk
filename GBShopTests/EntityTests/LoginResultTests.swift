//
//  LoginResultTests.swift
//  GBShopTests
//
//  Created by Тигран on 03/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
@testable import GBShop

class LoginResultTests: XCTestCase {
	
	func testAllFieldsParse() {
		let object = """
			{
				"result" : 1,
				"user" : {
							"id_user": 1,
							"user_login": "username",
							"user_name": "Tigran",
							"user_lastname": "Khachaturian",
							"email": "123@mail.ru",
							"is_admin": true,
							"gender": "m",
							"credit_card": "1234567890123456",
							"bio": "Что-то обо мне!"
						},
				"authToken" : "some_authorizaion_token"
			}
		"""
		let data = object.data(using: .utf8)!
		
		do {
			let result = try JSONDecoder().decode(LoginResult.self, from: data)
			XCTAssertEqual(result.result, 1)
			XCTAssertEqual(result.user.id, 1)
			XCTAssertEqual(result.user.login, "username")
			XCTAssertEqual(result.user.firstname, "Tigran")
			XCTAssertEqual(result.user.lastname, "Khachaturian")
			XCTAssertEqual(result.user.email, "123@mail.ru")
			XCTAssertEqual(result.user.isAdmin, true)
			XCTAssertEqual(result.user.gender, "m")
			XCTAssertEqual(result.user.creditCard, "1234567890123456")
			XCTAssertEqual(result.user.bio, "Что-то обо мне!")
		} catch {
			XCTFail(error.localizedDescription)
		}
	}
	
}
