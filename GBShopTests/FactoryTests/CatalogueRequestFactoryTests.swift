//
//  CatalogRequestFactoryTests.swift
//  GBShopTests
//
//  Created by Тигран on 02/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
import Alamofire
import OHHTTPStubs
@testable import GBShop

final class CatalogRequestFactoryTests: XCTestCase {
	
	var factory: CatalogRequestFactoryImp!
	var errorHandler: ErrorHandlerMock!
	var errorParser: ErrorParserMock!
	var sessionManager: SessionManager!
	
	override func setUp() {
		errorHandler = ErrorHandlerMock(nil)
		errorParser = ErrorParserMock()
		let config = URLSessionConfiguration.ephemeral
		OHHTTPStubs.setEnabled(true, for: config)
		sessionManager = SessionManager(configuration: config)
		
		factory = CatalogRequestFactoryImp(errorHandler: errorHandler,
											 errorParser: errorParser,
											 sessionManager: sessionManager)
		
		super.setUp()
	}
	
	override func tearDown() {
		removeAllHTTPStubs()
		
		super.tearDown()
	}
	
	func testShouldLoadCatalogue() {
		let expectation = self.expectation(description: "Catalogue downloaded")
		stubHTTPResponse(method: .get, fileName: "catalogueStub", path: "/getCatalogue", statusCode: 200)
		var result: Int?
		
		factory.getProducts { (response) in
			result = response.count
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertNotNil(result)
	}
	
	func testShouldLoadProduct() {
		let expectation = self.expectation(description: "Product downloaded")
		stubHTTPResponse(method: .get, fileName: "productStub", path: "/getProduct", statusCode: 200)
		var result: Int?
		
		factory.getProductBy(id: 1) { (response) in
			result = response.result
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertEqual(result, 1)
	}

}
