//
//  BasketRequestFactoryTests.swift
//  GBShopTests
//
//  Created by Тигран on 14/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//
import XCTest
import Alamofire
import OHHTTPStubs
@testable import GBShop

final class BasketRequestFactoryTests: XCTestCase {
	
	var factory: BasketRequestFactory!
	var errorHandler: ErrorHandlerMock!
	var errorParser: ErrorParserMock!
	var sessionManager: SessionManager!
	var user: UserProtocol!
	
	override func setUp() {
		errorHandler = ErrorHandlerMock(nil)
		errorParser = ErrorParserMock()
		let config = URLSessionConfiguration.ephemeral
		OHHTTPStubs.setEnabled(true, for: config)
		sessionManager = SessionManager(configuration: config)
		user = NormalUserStub()
		
		factory = BasketRequestFactoryImp(errorHandler: errorHandler,
										  errorParser: errorParser,
										  sessionManager: sessionManager)
		
		super.setUp()
	}
	
	override func tearDown() {
		removeAllHTTPStubs()
		
		super.tearDown()
	}
	
	func testShouldAddToBasket() {
		let expectation = self.expectation(description: "Product`ve been added to basket")
		stubHTTPResponse(method: .post, fileName: "simpleStub", path: "/addToBasket", statusCode: 200)
		var result: Int?
		
		factory.addProduct(id: 123, quantity: 2) { (response) in
			result = response.result
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertEqual(result, 1)
	}
	
	func testShouldDeleteFromBasket() {
		let expectation = self.expectation(description: "Product`ve been deleted from basket")
		stubHTTPResponse(method: .delete, fileName: "simpleStub", path: "/deleteFromBasket", statusCode: 200)
		var result: Int?
		
		factory.deleteProduct(id: 123) { (response) in
			result = response.result
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertEqual(result, 1)
	}
	
	func testShouldGetBasket() {
		let expectation = self.expectation(description: "Basket received")
		stubHTTPResponse(method: .get, fileName: "basketStub", path: "/getBasket", statusCode: 200)
		var result: Int?
		
		factory.getBasket(user: user) { (response) in
			result = response.amount
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertNotNil(result)
	}
	
	func testShouldPay() {
		let expectation = self.expectation(description: "Payment succeed")
		stubHTTPResponse(method: .post, fileName: "messageStub", path: "/pay", statusCode: 200)
		var result: Int?
		
		factory.pay(creditCardNumber: "1234567890123456") { (response) in
			result = response.result
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertEqual(result, 1)
	}
	
}
