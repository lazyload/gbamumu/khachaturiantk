//
//  AuthRequestFactoryTests.swift
//  GBShopTests
//
//  Created by Тигран on 02/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
import Alamofire
import OHHTTPStubs
@testable import GBShop

final class AuthRequestFactoryTests: XCTestCase {
	
	var factory: AuthRequestFactoryImp!
	var errorHandler: ErrorHandlerMock!
	var errorParser: ErrorParserMock!
	var sessionManager: SessionManager!
	var user: UserProtocol!

    override func setUp() {
		errorHandler = ErrorHandlerMock(nil)
		errorParser = ErrorParserMock()
		let config = URLSessionConfiguration.ephemeral
		OHHTTPStubs.setEnabled(true, for: config)
		sessionManager = SessionManager(configuration: config)
		user = NormalUserStub()
		
		factory = AuthRequestFactoryImp(errorHandler: errorHandler, errorParser: errorParser, sessionManager: sessionManager)
		
		super.setUp()
    }

    override func tearDown() {
        removeAllHTTPStubs()
		
		super.tearDown()
    }

	func testLoginShouldSucceed() {
		let expectation = self.expectation(description: "Login succeed")
		stubHTTPResponse(method: .post, fileName: "loginStub", path: "/login", statusCode: 200)
		var result: Int?
		
		factory.login(userName: "username", password: "123456") { (response) in
			result = response.result
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertEqual(result, 1)
	}
	
	func testLogoutShouldSucceed() {
		let expectation = self.expectation(description: "Logout succeed")
		stubHTTPResponse(method: .post, fileName: "simpleStub", path: "/logout", statusCode: 200)
		var result: Int?
		
		factory.logout(userId: 123) { (response) in
			result = response.result
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertEqual(result, 1)
	}
	
	func testRegisterShouldSucceed() {
		let expectation = self.expectation(description: "Registration succeed")
		stubHTTPResponse(method: .post, fileName: "messageStub", path: "/register", statusCode: 200)
		var result: Int?
		factory.register(user: user) { (response) in
			result = response.result
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertEqual(result, 1)
	}
	
}
