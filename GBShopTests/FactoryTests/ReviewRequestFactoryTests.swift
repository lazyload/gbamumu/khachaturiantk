//
//  ReviewRequestFactoryTests.swift
//  GBShopTests
//
//  Created by Тигран on 08/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
import Alamofire
import OHHTTPStubs
@testable import GBShop

final class ReviewRequestFactoryTests: XCTestCase {

	var factory: ReviewRequestFactory!
	var errorHandler: ErrorHandlerMock!
	var errorParser: ErrorParserMock!
	var sessionManager: SessionManager!
	var user: UserProtocol!
	
	override func setUp() {
		errorHandler = ErrorHandlerMock(nil)
		errorParser = ErrorParserMock()
		let config = URLSessionConfiguration.ephemeral
		OHHTTPStubs.setEnabled(true, for: config)
		sessionManager = SessionManager(configuration: config)
		user = AdminUserStub()
		
		factory = ReviewRequestFactoryImp(errorHandler: errorHandler,
											 errorParser: errorParser,
											 sessionManager: sessionManager)
		
		super.setUp()
	}
	
	override func tearDown() {
		removeAllHTTPStubs()
		
		super.tearDown()
	}
	
	func testShouldAddReview() {
		let expectation = self.expectation(description: "Review send on moderation")
		stubHTTPResponse(method: .post, fileName: "messageStub", path: "/addReview", statusCode: 200)
		var result: Int?
		
		factory.addReview(user: user, text: "Abrakadabra") { (response) in
			result = response.result
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertNotNil(result)
	}
	
	func testShouldDeleteReview() {
		let expectation = self.expectation(description: "Review deleted")
		stubHTTPResponse(method: .delete, fileName: "simpleStub", path: "/deleteReview", statusCode: 200)
		var result: Int?
		
		factory.deleteReview(idComment: 123) { (response) in
			result = response.result
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertNotNil(result)
	}
	
	func testShouldApproveReview() {
		let expectation = self.expectation(description: "Review approved")
		stubHTTPResponse(method: .post, fileName: "simpleStub", path: "/approveReview", statusCode: 200)
		var result: Int?
		
		factory.approveReview(idComment: 123, user: user) { (response) in
			result = response.result
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertNotNil(result)
	}

}
