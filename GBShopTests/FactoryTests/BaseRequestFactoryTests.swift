//
//  BaseRequestFactoryTests.swift
//  GBShopTests
//
//  Created by Тигран on 02/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
import Alamofire
import OHHTTPStubs
@testable import GBShop

final class BaseRequestFactoryTests: XCTestCase {
	
	var errorHandler: ErrorHandlerMock!
	var errorParser: ErrorParserMock!
	var sessionManager: SessionManager!
	var factory: BaseRequestFactory?

    override func setUp() {
		errorHandler = ErrorHandlerMock(nil)
		errorParser = ErrorParserMock()
		let config = URLSessionConfiguration.ephemeral
		OHHTTPStubs.setEnabled(true, for: config)
		sessionManager = SessionManager(configuration: config)
		
		factory = BaseRequestFactory(errorHandler: errorHandler, errorParser: errorParser, sessionManager: sessionManager)
		
		super.setUp()
    }

    override func tearDown() {
		removeAllHTTPStubs()
		
        super.tearDown()
    }
	
	func testFactoryShouldInit() {
		let expectation = self.expectation(description: "Init succeed")
		expectation.fulfill()
		wait(for: [expectation], timeout: 5)
		XCTAssertNotNil(factory)
	}
	
	func testShouldParseResponse() {
		let expectation = self.expectation(description: "Parse response succeed")
		stubHTTPResponse(method: .post, fileName: "loginStub", path: "/login", statusCode: 200)
		var user: UserResultStub?
		let routeStub = LoginRequestStub(baseUrl: factory!.baseUrl)
		factory?.request(request: routeStub, completionHandler: { (result: LoginResultStub) in
			user = result.user
			expectation.fulfill()
		})
		wait(for: [expectation], timeout: 5)
		XCTAssertNotNil(user)
	}
	
	func testShouldNotParseResponse() {
		let expectation = self.expectation(description: "Parse response failed")
		stubHTTPResponse(method: .post, fileName: "wrongLoginStub", path: "/login", statusCode: 200)
		var user: UserResultStub?
		
		let expErrorHandler = ErrorHandlerMock(expectation)
		factory = BaseRequestFactory(errorHandler: expErrorHandler, errorParser: errorParser, sessionManager: sessionManager)
		
		let routeStub = LoginRequestStub(baseUrl: factory!.baseUrl)
		factory?.request(request: routeStub, completionHandler: { (result: LoginResultStub) in
			user = result.user
		})
		wait(for: [expectation], timeout: 5)
		XCTAssertNil(user)
	}
	
	func testShouldGetFailResult() {
		let expectation = self.expectation(description: "Response with .failure result")
		stubHTTPResponse(method: .get, fileName: "", path: "/login", statusCode: 500)
		var user: UserResultStub?
		
		let expErrorHandler = ErrorHandlerMock(expectation)
		factory = BaseRequestFactory(errorHandler: expErrorHandler, errorParser: errorParser, sessionManager: sessionManager)
		
		let routeStub = LoginRequestStub(baseUrl: factory!.baseUrl)
		factory?.request(request: routeStub, completionHandler: { (result: LoginResultStub) in
			user = result.user
		})
		wait(for: [expectation], timeout: 5)
		XCTAssertNil(user)
	}
	
}
