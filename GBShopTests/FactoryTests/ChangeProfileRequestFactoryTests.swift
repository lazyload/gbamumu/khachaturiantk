//
//  ChangeProfileRequestFactoryTests.swift
//  GBShopTests
//
//  Created by Тигран on 02/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
import Alamofire
import OHHTTPStubs
@testable import GBShop

final class ChangeProfileRequestFactoryTests: XCTestCase {

	var factory: ChangeProfileRequestFactoryImp!
	var errorHandler: ErrorHandlerMock!
	var errorParser: ErrorParserMock!
	var sessionManager: SessionManager!
	var user: UserProtocol!
	
	override func setUp() {
		errorHandler = ErrorHandlerMock(nil)
		errorParser = ErrorParserMock()
		let config = URLSessionConfiguration.ephemeral
		OHHTTPStubs.setEnabled(true, for: config)
		sessionManager = SessionManager(configuration: config)
		user = NormalUserStub()
		
		factory = ChangeProfileRequestFactoryImp(errorHandler: errorHandler,
												 errorParser: errorParser,
												 sessionManager: sessionManager)
		
		super.setUp()
	}
	
	override func tearDown() {
		removeAllHTTPStubs()
		
		super.tearDown()
	}

	func testShouldChangeProfile() {
		let expectation = self.expectation(description: "Profile changed")
		stubHTTPResponse(method: .post, fileName: "simpleStub", path: "/changeUserData", statusCode: 200)
		var result: Int?
		
		factory.change(user: user) { (response) in
			result = response.result
			expectation.fulfill()
		}
		wait(for: [expectation], timeout: 5)
		XCTAssertEqual(result, 1)
	}

}
