//
//  LoginResultStub.swift
//  GBShopTests
//
//  Created by Тигран on 08/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

struct LoginResultStub: Codable {
	
	let result: Int
	let user: UserResultStub
	
}
