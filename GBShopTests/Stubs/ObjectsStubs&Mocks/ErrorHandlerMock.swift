//
//  ErrorHandlerMock.swift
//  GBShopTests
//
//  Created by Тигран on 08/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
@testable import GBShop

struct ErrorHandlerMock: AbstractErrorHandler {
	
	var expectation: XCTestExpectation?
	
	init(_ expectation: XCTestExpectation?) {
		self.expectation = expectation
	}
	
	func handle(error: AppError) {
		print(error.localizedDescription)
		self.expectation?.fulfill()
	}
	
}
