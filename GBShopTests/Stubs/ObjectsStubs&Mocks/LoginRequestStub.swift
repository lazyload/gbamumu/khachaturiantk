//
//  LoginRequestStub.swift
//  GBShopTests
//
//  Created by Тигран on 08/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Alamofire
@testable import GBShop

struct LoginRequestStub: RequestRouter {
	
	let baseUrl: URL
	let method: HTTPMethod = .post
	let path: String = "login"
	let parameters: Parameters? = [
		"username": "username",
		"password": "123456"
	]
	
}
