//
//  ApiErrorStub.swift
//  GBShopTests
//
//  Created by Тигран on 08/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

enum ApiErrorStub: Error {
	
	case fatalError
	
}
