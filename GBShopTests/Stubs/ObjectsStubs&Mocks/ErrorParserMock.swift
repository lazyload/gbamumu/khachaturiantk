//
//  ErrorParserMock.swift
//  GBShopTests
//
//  Created by Тигран on 08/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Alamofire
@testable import GBShop

struct ErrorParserMock: AbstractErrorParser {
	
	var error: AppError?
	
	func parse(_ result: Error) -> AppError {
		return error ?? .undefinedError(result.localizedDescription)
	}
	
	func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> AppError? {
		return self.error
	}
	
}
