//
//  AdminUserStub.swift
//  GBShopTests
//
//  Created by Тигран on 08/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation
@testable import GBShop

struct AdminUserStub: UserProtocol {
	
	let userId: Int = 123
	let username: String = "username"
	var password: String = "password"
	var email: String = "email"
	var isAdmin: Bool = true
	var firstname: String? = "Tigran"
	var lastname: String? = "Khachaturian"
	var gender: String? = "m"
	var creditCard: String? = "creditCard"
	var bio: String? = "bio"
	
}
