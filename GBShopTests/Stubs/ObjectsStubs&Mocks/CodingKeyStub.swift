//
//  CodingKeyStub.swift
//  GBShopTests
//
//  Created by Тигран on 08/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

struct CodingKeyStub: CodingKey {
	
	var stringValue: String
	var intValue: Int?
	
	init?(stringValue: String) {
		self.stringValue = stringValue
	}
	init?(intValue: Int) {
		self.intValue = intValue
		self.stringValue = intValue.description
	}
	
}
