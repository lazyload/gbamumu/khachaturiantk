//
//  UserResultStub.swift
//  GBShopTests
//
//  Created by Тигран on 08/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

struct UserResultStub: Codable {
	
	let id: Int
	let login: String
	let firstname: String
	let lastname: String
	
	enum CodingKeys: String, CodingKey {
		
		case id = "id_user"
		case login = "user_login"
		case firstname = "user_name"
		case lastname = "user_lastname"
		
	}
	
	func description() -> String {
		return "User id: \(id)\nUser name: \(firstname) \(lastname)"
	}
	
}
