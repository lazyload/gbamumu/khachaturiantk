//
//  HelpersAssemblyTests.swift
//  GBShopTests
//
//  Created by Тигран on 26/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest
@testable import GBShop

final class HelpersAssemblyTests: XCTestCase {

	func testShouldReturnAlertCenter() {
		let alertCenter = di.resolve(AlertCenter.self)
		XCTAssertNotNil(alertCenter)
	}
	
	func testShouldReturnViewDecorator() {
		let viewDecorator = di.resolve(ViewDecorator.self)
		XCTAssertNotNil(viewDecorator)
	}
	
	func testShouldReturnNumberConverter() {
		let numberConverter = di.resolve(NumberConverter.self)
		XCTAssertNotNil(numberConverter)
	}

}
