//
//  AuthController.swift
//  COpenSSL
//
//  Created by Тигран on 06/11/2018.
//

import Foundation
import PerfectHTTP

/// Отвечает за обработку запросов на login, logout, register
final class AuthController {
	
	let login: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		guard let stringPostBody = request.postBodyString,
			let encodedPostBody = stringPostBody.data(using: .utf8) else {
				let error = HTTPResponseError(status: .internalServerError, description: "Wrong user data")
				response.handleError(error: error)
				response.completed()
				return
		}
		
		do {
			let requestObject = try JSONDecoder().decode(LoginRequestObject.self, from: encodedPostBody)
			
			if requestObject.username == "username" && requestObject.password == "123456" {
				try response.setBody(json: ["result": 1,
											"user": ["id_user": 1,
													 "user_login": "username",
													 "user_name": "Тигран",
													 "user_lastname": "Хачатурян",
													 "email": "123@mail.ru",
													 "is_admin": true,
													 "gender": "Мужской",
													 "credit_card": "1234-5678-9012-3456",
													 "bio": "Что-то обо мне!"]
													])
				response.completed()
			} else {
				let error = HTTPResponseError(status: .forbidden, description: "Неправильная пара логин/пароль.")
				response.handleError(error: error)
				response.completed()
			}
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Parse data error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
	let logout: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		guard let stringPostBody = request.postBodyString,
			let encodedPostBody = stringPostBody.data(using: .utf8) else {
				let error = HTTPResponseError(status: .internalServerError, description: "Wrong user data")
				response.handleError(error: error)
				response.completed()
				return
		}
		
		do {
			let requestObject = try JSONDecoder().decode(LogoutRequestObject.self, from: encodedPostBody)
			
			try response.setBody(json: ["result": 1])
			response.completed()
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Parse data error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
	let register: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		guard let stringPostBody = request.postBodyString,
			let encodedPostBody = stringPostBody.data(using: .utf8) else {
				let error = HTTPResponseError(status: .internalServerError, description: "Wrong user data")
				response.handleError(error: error)
				response.completed()
				return
		}
		
		do {
			let requestObject = try JSONDecoder().decode(RegisterRequestObject.self, from: encodedPostBody)
			
			try response.setBody(json: ["result": 1, "userMessage": "Регистрация прошла успешно!"])
			response.completed()
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Parse data error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
}
