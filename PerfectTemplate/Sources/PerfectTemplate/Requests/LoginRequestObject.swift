//
//  LoginRequestObject.swift
//  COpenSSL
//
//  Created by Тигран on 06/11/2018.
//

import Foundation

struct LoginRequestObject: Codable {
	
	var username: String
	var password: String
	
}
