//
//  DeleteFromBasketRequestObject.swift
//  PerfectTemplate
//
//  Created by Тигран on 14/11/2018.
//
// swiftlint:disable identifier_name

import Foundation

struct DeleteFromBasketRequestObject: Codable {
	
	let id_product: Int
	
}
