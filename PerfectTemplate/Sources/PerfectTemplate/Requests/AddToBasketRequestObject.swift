//
//  AddToBasketRequestObject.swift
//  PerfectTemplate
//
//  Created by Тигран on 14/11/2018.
//
// swiftlint:disable identifier_name

import Foundation

struct AddToBasketRequestObject: Codable {
	
	let id_product: Int
	let quantity: Int
	
}
