//
//  PayRequestObject.swift
//  PerfectTemplate
//
//  Created by Тигран on 14/11/2018.
//

import Foundation

struct PayRequestObject: Codable {
	
	let creditCard: String
	
}
