//
//  AddReviewRequestObject.swift
//  PerfectTemplate
//
//  Created by Тигран on 08/11/2018.
//
// swiftlint:disable identifier_name

import Foundation

struct AddReviewRequestObject: Codable {
	
	let id_user: Int
	let text: String
	
}
