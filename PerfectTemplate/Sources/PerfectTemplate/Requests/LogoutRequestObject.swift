//
//  LogoutRequestObject.swift
//  COpenSSL
//
//  Created by Тигран on 06/11/2018.
//
// swiftlint:disable identifier_name

import Foundation

struct LogoutRequestObject: Codable {
	
	var id_user: Int
	
}
