//
//  DeleteReviewRequestObject.swift
//  PerfectTemplate
//
//  Created by Тигран on 08/11/2018.
//
// swiftlint:disable identifier_name

import Foundation

struct DeleteReviewRequestObject: Codable {
	
	let id_comment: Int
	
}
