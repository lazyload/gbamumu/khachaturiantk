//
//  ApproveReviewRequestObject.swift
//  PerfectTemplate
//
//  Created by Тигран on 08/11/2018.
//
// swiftlint:disable identifier_name

import Foundation

struct ApproveReviewRequestObject: Codable {
	
	let id_comment: Int
	let is_admin: Bool
	
}
