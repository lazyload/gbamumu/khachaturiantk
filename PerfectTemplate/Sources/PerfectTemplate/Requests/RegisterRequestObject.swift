//
//  RegisterRequestObject.swift
//  COpenSSL
//
//  Created by Тигран on 06/11/2018.
//
// swiftlint:disable identifier_name

import Foundation

struct RegisterRequestObject: Codable {
	
	var id_user: Int
	var username: String
	var password: String
	var email: String
	var gender: String?
	var credit_card: String?
	var bio: String?
	
}
