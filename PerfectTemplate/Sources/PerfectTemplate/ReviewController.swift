//
//  ReviewController.swift
//  PerfectTemplate
//
//  Created by Тигран on 08/11/2018.
//

import Foundation
import PerfectHTTP

/// Отвечает за обработку запросов по отзывам.
/// Обрабатывает добавлние нового отзыва, удаление отзыва и публикацию отзыва (требуются права администратора)
final class ReviewController {
	
	let addReview: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		guard let stringPostBody = request.postBodyString,
			  let encodedPostBody = stringPostBody.data(using: .utf8) else {
				let error = HTTPResponseError(status: .internalServerError, description: "Wrong user data")
				response.handleError(error: error)
				response.completed()
				return
		}
		
		do {
			let requestObject = try JSONDecoder().decode(AddReviewRequestObject.self, from: encodedPostBody)
			
			try response.setBody(json: ["result": 1, "userMessage" : "Отзыв отправлен на модерацию."])
			response.completed()
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Parse data error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
	let deleteReview: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		guard let stringPostBody = request.postBodyString,
			let encodedPostBody = stringPostBody.data(using: .utf8) else {
				let error = HTTPResponseError(status: .internalServerError, description: "Wrong user data")
				response.handleError(error: error)
				response.completed()
				return
		}
		
		do {
			let requestObject = try JSONDecoder().decode(DeleteReviewRequestObject.self, from: encodedPostBody)
			
			try response.setBody(json: ["result": 1])
			response.completed()
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Parse data error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
	let approveReview: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		guard let stringPostBody = request.postBodyString,
			let encodedPostBody = stringPostBody.data(using: .utf8) else {
				let error = HTTPResponseError(status: .internalServerError, description: "Wrong user data")
				response.handleError(error: error)
				response.completed()
				return
		}
		
		do {
			let requestObject = try JSONDecoder().decode(ApproveReviewRequestObject.self, from: encodedPostBody)
			
			guard requestObject.is_admin else {
				let error = HTTPResponseError(status: .forbidden, description: "У вас нет прав администратора.")
				response.handleError(error: error)
				response.completed()
				return
			}
			
			try response.setBody(json: ["result": 1])
			response.completed()
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Parse data error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
}
