//
//  BasketController.swift
//  COpenSSL
//
//  Created by Тигран on 14/11/2018.
//

import Foundation
import PerfectHTTP

/// Отвечает за обработку запросов в корзину.
/// Обрабатывает добавление товара в корзину, удаление, запрос на получение корзины и оплату.
final class BasketController {
	
	let addToBasket: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		guard let stringPostBody = request.postBodyString,
			let encodedPostBody = stringPostBody.data(using: .utf8) else {
				let error = HTTPResponseError(status: .internalServerError, description: "Wrong user data")
				response.handleError(error: error)
				response.completed()
				return
		}
		
		do {
			let requestObject = try JSONDecoder().decode(AddToBasketRequestObject.self, from: encodedPostBody)
			
			try response.setBody(json: ["result": 1])
			response.completed()
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Parse data error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
	let deleteFromBasket: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		guard let stringPostBody = request.postBodyString,
			let encodedPostBody = stringPostBody.data(using: .utf8) else {
				let error = HTTPResponseError(status: .internalServerError, description: "Wrong user data")
				response.handleError(error: error)
				response.completed()
				return
		}
		
		do {
			let requestObject = try JSONDecoder().decode(DeleteFromBasketRequestObject.self, from: encodedPostBody)
			
			try response.setBody(json: ["result": 1])
			response.completed()
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Parse data error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
	let getBasket: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		do {
			try response.setBody(json: [
										"amount": 4660000,
										"countGoods": 2,
										"contents": [
											[
												"id_product": 123,
												"product_name": "Ноутбук",
												"price": 4560000,
												"quantity": 1
											],
											[
												"id_product": 456,
												"product_name": "Мышка",
												"price": 100000,
												"quantity": 1
											]
										]
									])
			response.completed()
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Response error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
	let pay: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		guard let stringPostBody = request.postBodyString,
			let encodedPostBody = stringPostBody.data(using: .utf8) else {
				let error = HTTPResponseError(status: .internalServerError, description: "Wrong user data")
				response.handleError(error: error)
				response.completed()
				return
		}
		
		do {
			let requestObject = try JSONDecoder().decode(PayRequestObject.self, from: encodedPostBody)
			
			try response.setBody(json: ["result": 1, "userMessage" : "Оплата прошла успешно."])
			response.completed()
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Parse data error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
}
