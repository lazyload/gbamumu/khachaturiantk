//
//  ProfileController.swift
//  COpenSSL
//
//  Created by Тигран on 06/11/2018.
//

import Foundation
import PerfectHTTP

/// Отвечает за обработку запросов на изменение профиля пользователя
final class ProfileController {
	
	let changeProfile: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		guard let stringPostBody = request.postBodyString,
			let encodedPostBody = stringPostBody.data(using: .utf8) else {
				let error = HTTPResponseError(status: .internalServerError, description: "Wrong user data")
				response.handleError(error: error)
				response.completed()
				return
		}
		
		do {
			let requestObject = try JSONDecoder().decode(ChangeDataRequestObject.self, from: encodedPostBody)
			
			try response.setBody(json: ["result": 1])
			response.completed()
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Parse data error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
}
