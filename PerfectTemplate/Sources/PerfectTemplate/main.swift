//
//  main.swift
//  PerfectTemplate
//
//  Created by Kyle Jessup on 2015-11-05.
//	Copyright (C) 2015 PerfectlySoft, Inc.
//
//===----------------------------------------------------------------------===//
//
// This source file is part of the Perfect.org open source project
//
// Copyright (c) 2015 - 2016 PerfectlySoft Inc. and the Perfect project authors
// Licensed under Apache License v2.0
//
// See http://perfect.org/licensing.html for license information
//
//===----------------------------------------------------------------------===//
//

import PerfectHTTP
import PerfectHTTPServer

// Создание необходимых объектов для сервера
let server = HTTPServer()
let authController = AuthController()
let profileController = ProfileController()
let catalogueController = CatalogueController()
let reviewController = ReviewController()
let basketController = BasketController()
var routes = Routes()

// Добавление URI маршрутов
routes.add(method: .post, uri: "/login", handler: authController.login)
routes.add(method: .post, uri: "/logout", handler: authController.logout)
routes.add(method: .post, uri: "/register", handler: authController.register)
routes.add(method: .post, uri: "/changeUserData", handler: profileController.changeProfile)
routes.add(method: .get, uri: "/getCatalogue", handler: catalogueController.getCatalogue)
routes.add(method: .get, uri: "/getProduct", handler: catalogueController.getProduct)
routes.add(method: .post, uri: "/addReview", handler: reviewController.addReview)
routes.add(method: .delete, uri: "/deleteReview", handler: reviewController.deleteReview)
routes.add(method: .post, uri: "/approveReview", handler: reviewController.approveReview)
routes.add(method: .post, uri: "/addToBasket", handler: basketController.addToBasket)
routes.add(method: .delete, uri: "/deleteFromBasket", handler: basketController.deleteFromBasket)
routes.add(method: .get, uri: "/getBasket", handler: basketController.getBasket)
routes.add(method: .post, uri: "/pay", handler: basketController.pay)

// Запуск сервера
try HTTPServer.launch(name: "localhost",
					  port: 3008,
					  routes: routes,
					  responseFilters: [
						(PerfectHTTPServer.HTTPFilter.contentCompression(data: [:]), HTTPFilterPriority.high)])

