//
//  CatalogueController.swift
//  COpenSSL
//
//  Created by Тигран on 06/11/2018.
//

import Foundation
import PerfectHTTP

/// Отвечает за обработку запросов на получение данных о товарах.
/// Обрабатывает запросы getCatalogue и getProduct
final class CatalogueController {
	
	let getCatalogue: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		do {
			try response.setBody(json: [
				["id_product": 123, "product_name": "Ноутбук", "price": 4560000],
				["id_product": 456, "product_name": "Мышка", "price": 100000]
				])
			response.completed()
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Response error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
	let getProduct: (HTTPRequest, HTTPResponse) -> Void = { request, response in
		let productID = request.param(name: "id_product")
		
		guard let id = productID else {
			let error = HTTPResponseError(status: .badRequest, description: "Wrong id_product field")
			response.handleError(error: error)
			response.completed()
			return
		}
		
		do {
			switch id {
			case "123":
				try response.setBody(json: [
					"result": 1,
					"product_name": "Ноутбук",
					"product_price": 4560000,
					"product_description": "Мощный игровой ноутбук"
					])
				response.completed()
			case "456":
				try response.setBody(json: [
					"result": 1,
					"product_name": "Мышка",
					"product_price": 100000,
					"product_description": "Лучшая игровая мыша"
					])
				response.completed()
			default:
				let error = HTTPResponseError(status: .notFound, description: "Продукт с идентификатором \(id) не найден.")
				response.handleError(error: error)
				response.completed()
			}
		} catch {
			let error = HTTPResponseError(status: .internalServerError, description: "Parse data error - \(error)")
			response.handleError(error: error)
			response.completed()
		}
	}
	
}
