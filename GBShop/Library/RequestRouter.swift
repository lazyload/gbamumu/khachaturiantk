//
//  RequestRouter.swift
//  GBShop
//
//  Created by Тигран on 29/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Alamofire

/// Кодировка параметров запроса.
///
/// - url: Параметры в URI запроса.
/// - json: Параметры в теле запроса.
enum RequestRouterEncoding {
	case url, json
}

/// Абстракция запроса в сеть.
protocol RequestRouter: URLRequestConvertible {
	
	var baseUrl: URL { get }
	var method: HTTPMethod { get }
	var path: String { get }
	var parameters: Parameters? { get }
	var fullUrl: URL { get }
	var encoding: RequestRouterEncoding { get }
	
}

extension RequestRouter {
	
	var fullUrl: URL {
		return baseUrl.appendingPathComponent(path)
	}
	
	var encoding: RequestRouterEncoding {
		return .url
	}
	
	func asURLRequest() throws -> URLRequest {
		var urlRequest = URLRequest(url: fullUrl)
		urlRequest.httpMethod = method.rawValue
		
		switch self.encoding {
		case .url:
			return try URLEncoding.default.encode(urlRequest, with: parameters)
		case .json:
			return try JSONEncoding.default.encode(urlRequest, with: parameters)
		}
	}
	
}
