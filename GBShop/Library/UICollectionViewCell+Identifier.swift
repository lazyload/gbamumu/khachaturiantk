//
//  UICollectionViewCell+Identifier.swift
//  GBShop
//
//  Created by Тигран on 26/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
	
	@objc class var identifier: String {
		return String(describing: self)
	}
	
}
