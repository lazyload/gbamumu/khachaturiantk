//
//  UIViewController+HideTabBar.swift
//  GBShop
//
//  Created by Тигран on 23/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

extension UIViewController {
	
	/// Прячет `tabBar`.
	///
	/// - Parameters:
	///   - animated: флаг анимирования.
	///   - duration: продолжительность анимации.
	func hideTabBar(_ animated: Bool = true, duration: TimeInterval = 0.3) {
		guard let tabBar = tabBarController?.tabBar,
			!tabBar.isHidden else { return }
		
		if animated {
			let frame = tabBar.frame
			let originY = frame.origin.y + frame.size.height
			let tabBarOrigin = CGPoint(x: frame.origin.x, y: originY)
			let tabBarSize = frame.size
			
			UIView.animate(withDuration: duration,
						   animations: {
							tabBar.frame = CGRect(origin: tabBarOrigin, size: tabBarSize)
			}, completion: { _ in
				tabBar.isHidden = true
			})
		} else {
			tabBar.isHidden = true
		}
	}
	
	/// Показывает `tabBar`.
	///
	/// - Parameters:
	///   - animated: флаг анимирования.
	///   - duration: продолжительность анимации.
	func showTabBar(_ animated: Bool = true, duration: TimeInterval = 0.3) {
		guard let tabBar = tabBarController?.tabBar,
			tabBar.isHidden else { return }
		
		if animated {
			let frame = tabBar.frame
			let originY = frame.origin.y - frame.size.height
			let tabBarOrigin = CGPoint(x: frame.origin.x, y: originY)
			let tabBarSize = frame.size
			
			UIView.animate(withDuration: duration,
						   animations: {
							tabBar.frame = CGRect(origin: tabBarOrigin, size: tabBarSize)
							tabBar.isHidden = false
			})
		} else {
			tabBar.isHidden = false
		}
	}
	
}
