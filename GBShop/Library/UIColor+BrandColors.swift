//
//  UIColor+BrandColors.swift
//  GBShop
//
//  Created by Тигран on 21/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

extension UIColor {
	
	class var brandBlue: UIColor {
		return UIColor(named: "BlueColor") ?? .blue
	}
	
	class var brandLightBlue: UIColor {
		return UIColor(named: "LightBlueColor") ?? .blue
	}
	
	class var brandGreen: UIColor {
		return UIColor(named: "GreenColor") ?? .blue
	}
	
	class var background: UIColor {
		return UIColor(named: "BackgroundColor") ?? .white
	}
	
	class var textColor: UIColor {
		return UIColor(named: "TextColor") ?? .blue
	}
	
	class var darkTextColor: UIColor {
		return UIColor(named: "DarkTextColor") ?? .blue
	}
	
	class var borderColor: UIColor {
		return UIColor(named: "BorderColor") ?? .blue
	}
	
	class var grayBackground: UIColor {
		return UIColor(named: "GrayBackground") ?? .gray
	}
	
}
