//
//  ProductDescriptionView.swift
//  GBShop
//
//  Created by Тигран on 22/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

/// Отображает описание продукта.
@IBDesignable
final class ProductDescriptionView: XibView {
	
	// MARK: - IBOutlet
	@IBOutlet weak private var descriptionLabel: UILabel!
	
	// MARK: - Public properties
	@IBInspectable
	var text: String {
		get {
			return descriptionLabel.text ?? ""
		}
		set {
			descriptionLabel.text = newValue
		}
	}
	
}
