//
//  HeaderView.swift
//  GBShop
//
//  Created by Тигран on 21/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

/// Текстовый лэйбл с логотипом.
@IBDesignable
final class HeaderView: XibView {
	
	// MARK: - Dependency
	private lazy var viewDecorator: ViewDecorator? = {
		return di.resolve(ViewDecorator.self)
	}()
	
	// MARK: - IBOutlet
	@IBOutlet weak var icon: UIImageView! {
		didSet {
			viewDecorator?.applyShadow(view: icon,
									   size: .small,
									   color: .black,
									   place: .bottom)
		}
	}
	
	@IBOutlet weak var titleLabel: UILabel!
	
}

// MARK: - IBInspectable wrappers
extension HeaderView {
	
	@IBInspectable
	var title: String {
		get {
			return titleLabel.text ?? ""
		}
		set {
			titleLabel.text = newValue
		}
	}

}
