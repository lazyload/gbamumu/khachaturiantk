//
//  RoundButtonWithShadow.swift
//  GBShop
//
//  Created by Тигран on 21/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

/// Объект, отображающий кнопку со скругленными краями и тенью. Также имеет анимацию нажатия.
final class RoundButtonWithShadow: UIButton {
	
	// MARK: - Dependency
	private lazy var viewDecorator: ViewDecorator? = {
		return di.resolve(ViewDecorator.self)
	}()
	
	// MARK: - Public properties
	override var isEnabled: Bool {
		didSet {
			if oldValue != isEnabled {
				updateAppearance()
			}
		}
	}
	
	override var isSelected: Bool {
		didSet {
			if oldValue != isSelected {
				updateAppearance()
			}
		}
	}
	
	override var isHighlighted: Bool {
		didSet {
			if oldValue != isHighlighted {
				updateAppearance()
			}
		}
	}
	
	// MARK: - Init
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		setupButton()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		setupButton()
	}
	
	// MARK: - Private methods
	private func updateAppearance() {
		if (isSelected || isHighlighted) && isEnabled {
			buttonTouchedIn()
		} else {
			buttonTouchedOut()
		}
		alpha = isEnabled ? 1 : 0.8
	}
	
	private func buttonTouchedIn() {
		UIView.animate(withDuration: 0.2) {
			self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
		}
	}
	
	private func buttonTouchedOut() {
		UIView.animate(withDuration: 0.2) {
			self.transform = CGAffineTransform.identity
		}
	}
	
	private func setupButton() {
		viewDecorator?.applySmoothCorners(view: self)
		viewDecorator?.applyShadow(view: self,
								   size: .big,
								   color: self.backgroundColor,
								   place: .bottom)
	}
	
}
