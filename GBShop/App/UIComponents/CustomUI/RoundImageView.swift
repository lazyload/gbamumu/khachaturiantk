//
//  RoundImageView.swift
//  GBShop
//
//  Created by Тигран on 24/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

/// Объект, отображающий круглое изображение.
final class RoundImageView: UIImageView {

	// MARK: - Dependency
	private lazy var viewDecorator: ViewDecorator? = {
		return di.resolve(ViewDecorator.self)
	}()
	
	// MARK: - Init
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		setupImage()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		setupImage()
	}

	// MARK: - Private methods
	private func setupImage() {
		viewDecorator?.applyRoundCorners(view: self)
	}
	
}
