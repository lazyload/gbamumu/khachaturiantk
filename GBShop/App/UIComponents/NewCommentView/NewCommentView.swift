//
//  NewCommentView.swift
//  GBShop
//
//  Created by Тигран on 30/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

protocol NewCommentProtocol: class {
	
	func commentSend(_ text: String)
	
}

@IBDesignable
final class NewCommentView: XibView {
	
	// MARK: - Dependency
	private lazy var viewDecorator: ViewDecorator? = {
		return di.resolve(ViewDecorator.self)
	}()
	
	// MARK: - Public properties
	weak var delegate: NewCommentProtocol?
	
	// MARK: - IBOutlet
	@IBOutlet weak private var container: UIView! {
		didSet {
			viewDecorator?.applySmoothCorners(view: container)
		}
	}
	
	@IBOutlet weak var commentTextView: UITextView!
	
	// MARK: - IBAction
	@IBAction func sentPressed(_ sender: Any?) {
	delegate?.commentSend(commentTextView.text)
	}

}
