//
//  BasketCell.swift
//  GBShop
//
//  Created by Тигран on 26/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

final class BasketCell: UITableViewCell {
	
	// MARK: - Dependency
	private lazy var viewDecorator: ViewDecorator? = {
		return di.resolve(ViewDecorator.self)
	}()
	
	// MARK: - IBOutlet
	@IBOutlet weak private var productImageView: UIImageView!
	@IBOutlet weak private var productNameLabel: UILabel!
	@IBOutlet weak private var productDetailsLabel: UILabel!
	@IBOutlet weak private var productPriceLabel: UILabel!
	@IBOutlet weak private var productCountLabel: UILabel!
	@IBOutlet weak private var conteiner: UIView! {
		didSet {
			viewDecorator?.applySmoothCorners(view: conteiner)
			viewDecorator?.applyShadow(view: conteiner,
									   size: .small,
									   color: .black,
									   place: .bottom)
		}
	}
	
	// MARK: - Public properties
	var productImage: UIImage? {
		get {
			return productImageView.image
		}
		set {
			productImageView.image = newValue
			productImageView.isHidden = newValue == nil
		}
	}
	
	var name: String {
		get {
			return productNameLabel.text ?? ""
		}
		set {
			productNameLabel.text = newValue
		}
	}
	
	var details: String {
		get {
			return productDetailsLabel.text ?? ""
		}
		set {
			productDetailsLabel.text = newValue
		}
	}
	
	var price: String {
		get {
			return productPriceLabel.text ?? ""
		}
		set {
			productPriceLabel.text = newValue
		}
	}
	
	var count: String {
		get {
			return productCountLabel.text ?? ""
		}
		set {
			productCountLabel.text = newValue
		}
	}
	
}
