//
//  ProductInfoView.swift
//  GBShop
//
//  Created by Тигран on 22/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

protocol ItemSelectedToBasket: class {
	
	func itemSelected()
	
}

/// Отображает информацию о продукте - картинку, название, стоимость и кнопку добавления в корзину.
@IBDesignable
final class ProductInfoView: XibView {
	
	// MARK: - Dependency
	private lazy var viewDecorator: ViewDecorator? = {
		return di.resolve(ViewDecorator.self)
	}()
	
	// MARK: - Public properties
	weak var delegate: ItemSelectedToBasket?
	
	// MARK: - IBOutlet
	@IBOutlet weak private var titleLabel: UILabel!
	@IBOutlet weak private var imageView: UIImageView! {
		didSet {
			viewDecorator?.applySmoothCorners(view: imageView)
			viewDecorator?.applyShadow(view: imageView,
									   size: .medium,
									   color: imageView.backgroundColor,
									   place: .bottom)
		}
	}
	
	@IBOutlet weak private var priceLabel: UILabel!
	@IBOutlet weak private var addButton: UIButton! {
		didSet {
			viewDecorator?.applyShadow(view: addButton,
									   size: .small,
									   color: .brandGreen,
									   place: .round)
		}
	}
	
	// MARK: - IBActions
	@IBAction func addTapped(_ sender: Any) {
		delegate?.itemSelected()
	}
	
}

// MARK: - IBInspectable wrappers
extension ProductInfoView {
	
	@IBInspectable
	var title: String {
		get {
			return titleLabel.text ?? ""
		}
		set {
			titleLabel.text = newValue
		}
	}
	
	@IBInspectable
	var image: UIImage? {
		get {
			return imageView.image
		}
		set {
			imageView.image = newValue?.withRenderingMode(.alwaysOriginal)
			imageView.isHidden = newValue == nil
		}
	}
	
	@IBInspectable
	var price: String {
		get {
			return priceLabel.text ?? ""
		}
		set {
			priceLabel.text = newValue
		}
	}
	
}
