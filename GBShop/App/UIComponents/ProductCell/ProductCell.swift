//
//  ProductCell.swift
//  GBShop
//
//  Created by Тигран on 20/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

/// Ячейка, отображающая информацию о продукте.
/// Содержит картинку, название и цену продукта, кнопку для добавления в корзину.
final class ProductCell: UICollectionViewCell {
	
	// MARK: - Dependency
	private lazy var viewDecorator: ViewDecorator? = {
		return di.resolve(ViewDecorator.self)
	}()
	
	// MARK: - IBOutlet
	@IBOutlet weak private var productImage: UIImageView!
	@IBOutlet weak private var productName: UILabel!
	@IBOutlet weak private var productPrice: UILabel!
	
	// MARK: - Public properties
	var image: UIImage? {
		get {
			return productImage.image
		}
		set {
			productImage.image = newValue
			productImage.isHidden = newValue == nil
		}
	}
	
	var name: String {
		get {
			return productName.text ?? ""
		}
		set {
			productName.text = newValue
		}
	}
	
	var price: String {
		get {
			return productPrice.text ?? ""
		}
		set {
			productPrice.text = newValue
		}
	}
	
}
