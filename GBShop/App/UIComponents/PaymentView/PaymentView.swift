//
//  PaymentView.swift
//  GBShop
//
//  Created by Тигран on 27/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

protocol PaymentProtocol: class {
	
	func didPressPay()
	
}

final class PaymentView: XibView {
	
	// MARK: - Public properties
	weak var delegate: PaymentProtocol?
	
	// MARK: - IBOutlets
	@IBOutlet weak private var cashButton: RoundButtonWithShadow!
	@IBOutlet weak private var cardButton: RoundButtonWithShadow!
	@IBOutlet weak private var applePayButton: RoundButtonWithShadow!

	// MARK: - IBAction
	@IBAction func cashPressed(_ sender: Any) {
		cashButton.setAttributedTitle(NSAttributedString(string: " Наличные",
														 attributes: [NSAttributedString.Key.font:
															UIFont.init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Semibold", size: 18), size: 18),
																	  NSAttributedString.Key.foregroundColor:
																		UIColor.darkTextColor]), for: .normal)
		
		cardButton.setAttributedTitle(NSAttributedString(string: " Карта",
														 attributes: [NSAttributedString.Key.font:
															UIFont.init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Regular", size: 18), size: 18),
																	  NSAttributedString.Key.foregroundColor:
																		UIColor.darkTextColor]), for: .normal)
		
		applePayButton.setAttributedTitle(NSAttributedString(string: " ApplePay",
														 attributes: [NSAttributedString.Key.font:
															UIFont.init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Regular", size: 18), size: 18),
																	  NSAttributedString.Key.foregroundColor:
																		UIColor.darkTextColor]), for: .normal)
	}
	
	@IBAction func cardPressed(_ sender: Any) {
		cashButton.setAttributedTitle(NSAttributedString(string: " Наличные",
														 attributes: [NSAttributedString.Key.font:
															UIFont.init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Regular", size: 18), size: 18),
																	  NSAttributedString.Key.foregroundColor:
																		UIColor.darkTextColor]), for: .normal)
		
		cardButton.setAttributedTitle(NSAttributedString(string: " Карта",
														 attributes: [NSAttributedString.Key.font:
															UIFont.init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Semibold", size: 18), size: 18),
																	  NSAttributedString.Key.foregroundColor:
																		UIColor.darkTextColor]), for: .normal)
		
		applePayButton.setAttributedTitle(NSAttributedString(string: " ApplePay",
															 attributes: [NSAttributedString.Key.font:
																UIFont.init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Regular", size: 18), size: 18),
																		  NSAttributedString.Key.foregroundColor:
																			UIColor.darkTextColor]), for: .normal)
	}
	
	@IBAction func applePayPressed(_ sender: Any) {
		cashButton.setAttributedTitle(NSAttributedString(string: " Наличные",
														 attributes: [NSAttributedString.Key.font:
															UIFont.init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Regular", size: 18), size: 18),
																	  NSAttributedString.Key.foregroundColor:
																		UIColor.darkTextColor]), for: .normal)
		
		cardButton.setAttributedTitle(NSAttributedString(string: " Карта",
														 attributes: [NSAttributedString.Key.font:
															UIFont.init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Regular", size: 18), size: 18),
																	  NSAttributedString.Key.foregroundColor:
																		UIColor.darkTextColor]), for: .normal)
		
		applePayButton.setAttributedTitle(NSAttributedString(string: " ApplePay",
															 attributes: [NSAttributedString.Key.font:
																UIFont.init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Semibold", size: 18), size: 18),
																		  NSAttributedString.Key.foregroundColor:
																			UIColor.darkTextColor]), for: .normal)
	}
	
	@IBAction func payPressed(_ sender: Any) {
		delegate?.didPressPay()
	}
	
}
