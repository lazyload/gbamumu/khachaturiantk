//
//  ProfileCell.swift
//  GBShop
//
//  Created by Тигран on 22/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

/// Форма с лэйблом и текстовым полем.
@IBDesignable
final class ProfileCell: XibView {
	
	// MARK: - IBOutlet
	@IBOutlet weak private var nameLabel: UILabel!
	@IBOutlet weak private var valueField: UITextField!

}

// MARK: - IBInspectable wrappers
extension ProfileCell {
	
	// MARK: - Public properties
	@IBInspectable
	var name: String {
		get {
			return nameLabel.text ?? ""
		}
		set {
			nameLabel.text = newValue
		}
	}
	
	@IBInspectable
	var value: String {
		get {
			return valueField.text ?? ""
		}
		set {
			valueField.text = newValue
		}
	}
	
	@IBInspectable
	var isSecured: Bool {
		get {
			return valueField.isSecureTextEntry
		}
		set {
			valueField.isSecureTextEntry = newValue
		}
	}
	
	@IBInspectable
	var hint: String {
		get {
			return valueField.attributedPlaceholder?.string ?? ""
		}
		set {
			let attributedHint = NSAttributedString(string: newValue,
													attributes: [.font: UIFont
														.init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Regular", size: 15), size: 15),
																 .foregroundColor: UIColor.textColor.withAlphaComponent(0.3)])
			valueField.attributedPlaceholder = attributedHint
		}
	}
	
}
