//
//  TextFieldView.swift
//  GBShop
//
//  Created by Тигран on 21/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

/// Форма, содержащая поле ввода, иконку и разделитель.
@IBDesignable
final class TextFieldView: XibView {
	
	// MARK: - IBOutlet
	@IBOutlet weak private var imageView: UIImageView! {
		didSet {
			if let image = imageView.image {
				imageView.image = image.withRenderingMode(.alwaysOriginal)
			}
		}
	}
	
	@IBOutlet weak private var titleLabel: UILabel!
	@IBOutlet weak private var textField: UITextField!
	@IBOutlet weak private var separator: UIView!
	@IBOutlet weak private var conteinerView: UIView! {
		didSet {
			setBorders()
		}
	}
	
	// MARK: - Private methods
	private func setBorders() {
		conteinerView.layer.borderColor = UIColor.borderColor.cgColor
		conteinerView.layer.borderWidth = 1.6
		conteinerView.layer.cornerRadius = 12
		conteinerView.layer.masksToBounds = true
	}
	
}

// MARK: - IBInspectable wrappers
extension TextFieldView {
	
	@IBInspectable
	var image: UIImage? {
		get {
			return imageView.image
		}
		set {
			imageView.image = newValue?.withRenderingMode(.alwaysTemplate)
			imageView.isHidden = newValue == nil
		}
	}
	
	@IBInspectable
	var title: String {
		get {
			return titleLabel.text ?? ""
		}
		set {
			titleLabel.text = newValue
		}
	}
	
	@IBInspectable
	var hint: String {
		get {
			return textField.attributedPlaceholder?.string ?? ""
		}
		set {
			let attributedHint = NSAttributedString(string: newValue,
													attributes: [.font: UIFont
														                .init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Regular", size: 18), size: 18),
																 .foregroundColor: UIColor.textColor.withAlphaComponent(0.3)])
			textField.attributedPlaceholder = attributedHint
		}
	}
	
	@IBInspectable
	var text: String {
		get {
			return textField.text ?? ""
		}
		set {
			textField.text = newValue
		}
	}
	
	@IBInspectable
	var isSecured: Bool {
		get {
			return textField.isSecureTextEntry
		}
		set {
			textField.isSecureTextEntry = newValue
		}
	}
	
}
