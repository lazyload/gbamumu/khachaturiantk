//
//  CommentCell.swift
//  GBShop
//
//  Created by Тигран on 22/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

/// Отображает комментарий о продукте.
/// Содержит аватарку, имя пользователя, лэйбл, когда был оставлен комментарий, лэйбл с самим комментарием.
final class CommentCell: UITableViewCell {
	
	// MARK: - IBOutlet
	@IBOutlet weak private var userImageView: RoundImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak private var commentLabel: UILabel!
	
	// MARK: - Public properties
	var userImage: UIImage? {
		get {
			return userImageView.image
		}
		set {
			userImageView.image = newValue?.withRenderingMode(.alwaysOriginal)
			userImageView.isHidden = newValue == nil
		}
	}
	
	var name: String {
		get {
			return nameLabel.text ?? ""
		}
		set {
			nameLabel.text = newValue
		}
	}
	
	var date: String {
		get {
			return dateLabel.text ?? ""
		}
		set {
			dateLabel.text = newValue
		}
	}
	
	var comment: String {
		get {
			return commentLabel.text ?? ""
		}
		set {
			commentLabel.text = newValue
		}
	}
	
}
