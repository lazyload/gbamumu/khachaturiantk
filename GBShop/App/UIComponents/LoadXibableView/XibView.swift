//
//  XibView.swift
//  UTair
//
//  Created by Евгений Елчев on 26.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

/// Класс, используемый для загрузки `xib` файлов. 
class XibView: UIView, LoadXibableView {

    public var xibView: UIView!
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }

}
