//
//  LoadXibableView.swift
//  UTair
//
//  Created by Евгений Елчев on 26.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

/// Отвечает за загрузку `xib`.
protocol LoadXibableView: class {
	
    var xibView: UIView! {get set}
	
}

extension LoadXibableView {
	
    /// Метод загрузки `xib`.
    ///
    /// - Returns: экземляр `UIView`, полученный из `xib`.
    public func loadViewFromNib() -> UIView? {
        let metaData = type(of: self)
        let bundle = Bundle.init(for: metaData)
        let nib = UINib(nibName: String(describing: metaData), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        return view
    }
	
}

extension LoadXibableView where Self: UIView {
	
	/// Производит настройку `xib` для корректного отображения на экране.
	func xibSetup() {
        xibView = loadViewFromNib()
        xibView.frame = bounds
        xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(xibView)
    }
	
}
