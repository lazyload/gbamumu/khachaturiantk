//
//  BasketFooter.swift
//  GBShop
//
//  Created by Тигран on 26/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

protocol BasketFooterDelegate: class {
	
	func checkoutPressed()
	
}

@IBDesignable
final class BasketFooter: XibView {
	
	// MARK: - Dependency
	private lazy var viewDecorator: ViewDecorator? = {
		return di.resolve(ViewDecorator.self)
	}()
	
	// MARK: - Public properties
	var isShowed: Bool = false
	weak var delegate: BasketFooterDelegate?
	
	// MARK: - IBOutlet
	@IBOutlet var conteiner: UIView! {
		didSet {
			viewDecorator?.applySmoothCorners(view: conteiner)
			viewDecorator?.applyShadow(view: conteiner,
									   size: .medium,
									   color: .black,
									   place: .top)
		}
	}
	
	@IBOutlet weak private var countLabel: UILabel! {
		didSet {
			viewDecorator?.applyRoundCorners(view: countLabel)
		}
	}
	
	@IBOutlet weak private var priceLabel: UILabel!
	@IBOutlet weak private var checkoutButton: RoundButtonWithShadow!
	
	// MARK: - IBAction
	@IBAction func checkoutPressed(_ sender: Any?) {
		delegate?.checkoutPressed()
	}
	
}

// MARK: - IBInspectable wrappers
extension BasketFooter {
	
	@IBInspectable
	var count: String {
		get {
			return countLabel.text ?? ""
		}
		set {
			countLabel.text = newValue
		}
	}
	
	@IBInspectable
	var price: String {
		get {
			return priceLabel.text ?? ""
		}
		set {
			priceLabel.text = newValue
		}
	}
	
}
