//
//  AbstractErrorHandler.swift
//  GBShop
//
//  Created by Тигран on 29/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Абстракция обработчика ошибок.
protocol AbstractErrorHandler {
	
	/// Отвечает за обработку ошибок.
	///
	/// - Parameter error: внутренний тип ошибки.
	func handle(error: AppError)
	
}
