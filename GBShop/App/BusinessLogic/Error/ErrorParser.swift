//
//  ErrorParser.swift
//  GBShop
//
//  Created by Тигран on 29/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Ошибки, возникающие в приложении.
///
/// - decodingError: ошибки, возникшие при декодировании.
/// - serverError: ошибки, произошедшие по причине сервера.
/// - clientError: ошибки, произошедшие по причине клиента (на сервере).
/// - networkError: ошибки, произошедшие в результате плохого соединения с сетью.
/// - undefinedError: прочие ошибки.
enum AppError: Error {
	
	case decodingError(String)
	case serverError(String)
	case clientError(String)
	case networkError(String)
	case undefinedError(String)
	
	var description: String {
		switch self {
		case .decodingError(let description):
			return description
		case .serverError(let description):
			return description
		case .networkError(let description):
			return description
		case .undefinedError(let description):
			return description
		case .clientError(let description):
			return description
		}
	}
	
}

/// Парсер ошибок. Содержит логику обработки полученных из сети ошибок.
protocol AbstractErrorParser {
	
	/// Отвечает за парсинг полученных ошибок в результате обработки загруженной информации.
	///
	/// - Parameter result: экземпляр ошибки.
	/// - Returns: внутренний тип ошибки.
	func parse(_ result: Error) -> AppError
	
	/// Отвечает за парсинг ошибок содержащихся в ответе с сервера.
	///
	/// - Parameters:
	///   - response: ответ сервера.
	///   - data: полученая информация.
	///   - error: полученая ошибка.
	/// - Returns: внутренний тип ошибки.
	func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> AppError?
	
}

final class ErrorParser: AbstractErrorParser {
	
	// MARK: - Public functions
	func parse(_ result: Error) -> AppError {
		return makeDecodingError(from: result)
			?? makeClientError(from: result)
			?? makeServerError(from: result)
			?? makeNetworkError(from: result)
			?? .undefinedError(result.localizedDescription)
	}
	
	func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> AppError? {
		if let error = error {
			return parse(error)
		}
		guard let response = response,
			 response.statusCode >= 299 else {
				return nil
		}
		return makeAppError(from: data)
	}
	
	// MARK: - Private functions
	private func makeDecodingError(from error: Error) -> AppError? {
		guard error is DecodingError else {
			return nil
		}
		return AppError.decodingError(error.localizedDescription)
	}
	
	private func makeServerError(from error: Error) -> AppError? {
		guard let error = error as NSError?,
			  error.code >= 500 && error.code <= 599 else {
			return nil
		}
		return AppError.serverError(error.domain)
	}
	
	private func makeClientError(from error: Error) -> AppError? {
		guard let error = error as NSError?,
			  error.code >= 400 && error.code <= 499 else {
			return nil
		}
		return AppError.clientError(error.domain)
	}
	
	private func makeNetworkError(from error: Error) -> AppError? {
		guard let error = error as NSError?,
			  error.code == -1001 || error.code == -1009 else {
			return nil
		}
		return AppError.networkError(error.domain)
	}
	
	private func makeAppError(from data: Data?) -> AppError {
		guard let data = data else {
			return .undefinedError("Отсутствует информация об ошибке.")
		}
		do {
			guard let errorDict = try JSONSerialization.jsonObject(with: data,
																   options: .mutableContainers) as? [String : Any],
				let errorDescription = errorDict["description"] as? String,
				let errorCode = errorDict["status"] as? Int else {
					return .undefinedError("Неизвестный формат ошибки.")
			}
			
			let error = NSError(domain: errorDescription, code: errorCode, userInfo: nil)
			return self.parse(error)
		} catch let error {
			return self.parse(error)
		}
	}
	
}
