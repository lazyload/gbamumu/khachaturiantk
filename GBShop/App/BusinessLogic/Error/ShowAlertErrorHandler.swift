//
//  ShowAlertErrorHandler.swift
//  GBShop
//
//  Created by Тигран on 19/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Обработчик ошибок. Как результат обработки - показ алерта с ошибкой.
final class ShowAlertErrorHandler: AbstractErrorHandler {
	
	// MARK: - Private properties
	let alertCenter: AlertCenter
	
	// MARK: - Init
	init(alertCenter: AlertCenter) {
		self.alertCenter = alertCenter
	}
	
	// MARK: - Public functions
	func handle(error: AppError) {
		DispatchQueue.main.async {
			self.alertCenter.showErrorAlert(withTitle: "",
											 message: error.description,
											 handler: nil)
		}
	}
	
}
