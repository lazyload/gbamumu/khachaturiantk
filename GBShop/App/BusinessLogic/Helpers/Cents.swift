//
//  Cents.swift
//  GBShop
//
//  Created by Тигран on 14/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Особый тип для учета всех цен в копейках для точности подсчетов.
typealias Cents = Int
