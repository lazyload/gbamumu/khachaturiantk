//
//  UserRepository.swift
//  GBShop
//
//  Created by Тигран on 30/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

protocol UserRepository {
	
	func load() -> UserProtocol?
	func save(_ user: UserProtocol)
	
}

final class UserRepositoryImp: UserRepository {
	
	private var user: UserProtocol?
	
	func load() -> UserProtocol? {
		return user
	}
	
	func save(_ user: UserProtocol) {
		self.user = user
	}
}

