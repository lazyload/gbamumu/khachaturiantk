//
//  AlertCenter.swift
//  GBShop
//
//  Created by Тигран on 15/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

/// Тип оповещений. От него зависит `action` алерта.
///
/// - error: оповещение об ошибке.
/// - notification: простое оповещение.
enum AlertType {
	case error
	case notification
}

/// Отвечает за показ различных оповещений.
protocol AlertCenter {
	
	/// Замыкание, вызываемое по нажатию на `action`.
	typealias ActionHandler = ((UIAlertAction) -> Void)?
	
	/// Показывает оповещение об ошибке.
	///
	/// - Parameters:
	///   - title: заголовок оповещения.
	///   - message: сообщение оповещения.
	///   - handler: замыкание, вызываемое по нажатию на `action`.
	func showErrorAlert(withTitle title: String?, message: String?, handler: ActionHandler)
	
	/// Показывает информационное оповещение.
	///
	/// - Parameters:
	///   - title: заголовок оповещения.
	///   - message: сообщение оповещения.
	///   - handler:  замыкание, вызываемое по нажатию на `action`.
	func showNotificationAlert(withTitle title: String?, message: String?, handler: ActionHandler)
	
	/// Показывает оповещение о внутренней ошибке.
	///
	/// - Parameter handler:  замыкание, вызываемое по нажатию на `action`.
	func showEnternalErrorAlert(handler: ActionHandler)
}

final class AlertCenterImp: AlertCenter {
	
	// MARK: - Public methods
	func showErrorAlert(withTitle title: String?, message: String?, handler: ActionHandler) {
		let alert = makeAlert(withTitle: title,
							  message: message,
							  actionHandler: handler,
							  type: .error)
		show(controller: alert)
	}
	
	func showNotificationAlert(withTitle title: String?, message: String?, handler: ActionHandler) {
		let alert = makeAlert(withTitle: title,
							  message: message,
							  actionHandler: handler,
							  type: .notification)
		show(controller: alert)
	}
	
	func showEnternalErrorAlert(handler: ActionHandler) {
		let alert = makeAlert(withTitle: "Ошибка",
							  message: "Внутренняя ошибка. Пожалуйста, обратитесь к разработчику.",
							  actionHandler: handler,
							  type: .error)
		show(controller: alert)
	}
	
	// MARK: - Private methods
	private func show(controller: UIAlertController) {
			if var topController = UIApplication.shared.keyWindow?.rootViewController {
				while let presentedViewController = topController.presentedViewController {
					topController = presentedViewController
				}
				topController.present(controller, animated: true, completion: nil)
			}
	}
	
	private func makeAlert(withTitle title: String?,
						   message: String?,
						   actionHandler handler: ((UIAlertAction) -> Void)?,
						   type: AlertType) -> UIAlertController {
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		
		switch type {
		case .error:
			let action = UIAlertAction(title: "Закрыть", style: .cancel, handler: handler)
			alert.addAction(action)
		case .notification:
			let action = UIAlertAction(title: "Ок", style: .cancel, handler: handler)
			alert.addAction(action)
		}
		return alert
	}
	
}
