//
//  ViewDecorator.swift
//  GBShop
//
//  Created by Тигран on 21/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

/// Тип размера тени.
///
/// - small: маленькая тень.
/// - medium: средняя тень.
/// - big: большая тень.
enum ShadowSize: CGFloat {
	case small = 2.5
	case medium = 5.0
	case big = 7.5
}

/// Тип тени.
///
/// - round: тень вокруг объекта.
/// - bottom: тень снизу объекта.
/// - top: тен сверху объекта.
enum ShadowType {
	case round
	case bottom
	case top
}

/// Отвечает за применение к `UIView` различных визуальных эффектов.
protocol ViewDecorator {
	
	/// Применяет тень.
	///
	/// - Parameters:
	///   - view: экземпляр `UIView`, к которому применяем эффект.
	///   - size: размер тени.
	///   - color: цвет тени.
	///   - place: тип тени.
	func applyShadow(view: UIView, size: ShadowSize, color: UIColor?, place: ShadowType)
	
	/// Делает `UIView` круглой.
	///
	/// - Parameter view: экземпляр `UIView`, к которому применяем эффект.
	func applyRoundCorners(view: UIView)
	
	/// Делает края `UIView` скругленными.
	///
	/// - Parameter view: экземпляр `UIView`, к которому применяем эффект.
	func applySmoothCorners(view: UIView)
	
}

final class ViewDecoratorImp: ViewDecorator {
	
	func applyShadow(view: UIView, size: ShadowSize, color: UIColor?, place: ShadowType) {
		view.layer.shadowColor = color?.cgColor ?? UIColor.black.cgColor
		
		view.layer.shadowRadius = size.rawValue
		view.layer.masksToBounds = false
		view.layer.shadowOpacity = 0.5
		
		switch place {
		case .round:
			view.layer.shadowOffset = CGSize.zero
		case .bottom:
			view.layer.shadowOffset = CGSize(width: 0, height: size.rawValue)
		case .top:
			view.layer.shadowOffset = CGSize(width: 0, height: -size.rawValue)
		}
	}
	
	func applyRoundCorners(view: UIView) {
		view.layer.cornerRadius = view.frame.size.width / 2
		view.layer.masksToBounds = true
	}
	
	func applySmoothCorners(view: UIView) {
		view.layer.cornerRadius = 12
		view.layer.masksToBounds = true
	}
	
}
