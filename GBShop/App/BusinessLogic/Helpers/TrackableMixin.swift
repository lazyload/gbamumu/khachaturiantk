//
//  TrackableMixin.swift
//  GBShop
//
//  Created by Тигран on 28/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//
// swiftlint:disable empty_enum_arguments

import Foundation
import YandexMobileMetrica

/// <#Description#>
///
/// - login: <#login description#>
/// - logout: <#logout description#>
/// - registration: <#registration description#>
/// - catalogViewed: <#catalogViewed description#>
/// - productViewed: <#productViewed description#>
/// - addedToBasket: <#addedToBasket description#>
/// - deletedFromBasket: <#deletedFromBasket description#>
/// - orderBeenPayed: <#orderBeenPayed description#>
/// - commentAdded: <#commentAdded description#>
enum AnaliticsEvent {
	case login(login: String, success: Bool)
	case logout
	case registration(username: String, email: String)
	case catalogViewed()
	case productViewed(id: Int)
	case addedToBasket(id: Int)
	case deletedFromBasket(id: Int)
	case orderBeenPayed()
	case commentAdded(text: String, username: String)
}

/// <#Description#>
protocol TrackableMixin {
	
	/// <#Description#>
	///
	/// - Parameter event: <#event description#>
	func track(_ event: AnaliticsEvent)
	
}

extension TrackableMixin {
	
	func track(_ event: AnaliticsEvent) {
		var message: String = ""
		var parameters: [AnyHashable: Any]?
		switch event {
		case let .login(login, success):
			message = "login"
			parameters = ["login": login, "success": success]
		case .logout:
			message = "logout"
		case let .registration(username, email):
			message = "registration"
			parameters = ["username": username, "email": email]
		case .catalogViewed():
			message = "catalog_viewed"
		case let .productViewed(id):
			message = "product_viewed"
			parameters = ["product_id": id]
		case let .addedToBasket(id):
			message = "added_to_basket"
			parameters = ["product_id": id]
		case let .deletedFromBasket(id):
			message = "deleted_from_basket"
			parameters = ["product_id": id]
		case .orderBeenPayed():
			message = "order_been_payed"
		case let .commentAdded(text, username):
			message = "comment_added"
			parameters = ["text_of_the_comment": text, "username": username]
		}
		
		YMMYandexMetrica.reportEvent(message,
									 parameters: parameters,
									 onFailure: nil)
	}
	
}
