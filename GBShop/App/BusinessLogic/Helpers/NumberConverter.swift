//
//  NumberConverter.swift
//  GBShop
//
//  Created by Тигран on 20/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Отвечает за конвертацию числа в строку.
protocol NumberConverter {
	
	/// Экземляр класса `NumberFormatter`.
	var formatter: NumberFormatter { get }
	
	/// Конвертирует цену в копейках в формат "123 р.".
	///
	/// - Parameter amount: цена в копейках.
	/// - Returns: строчное представление цены.
	func getPriceInRubles(_ amount: Cents?) -> String?
	
}

final class NumberConverterImp: NumberConverter {
	
	// MARK: - Public properties
	let formatter: NumberFormatter
	
	// MARK: - Init
	init(_ formatter: NumberFormatter) {
		self.formatter = formatter
		self.setupConverter()
	}
	
	// MARK: - Public methods
	func getPriceInRubles(_ amount: Cents?) -> String? {
		let rub = Double(amount ?? 0) / 100.0
		return formatter.string(for: rub)
	}
	
	// MARK: - Private methods
	private func setupConverter() {
		formatter.numberStyle = .currency
		formatter.allowsFloats = true
		formatter.locale = Locale.init(identifier: "ru_RU")
	}
	
}
