//
//  BaseRequestFactory.swift
//  GBShop
//
//  Created by Тигран on 29/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Alamofire

/// Базовый класс для выполнения запросов в сеть.
/// Содержит основную логику выполнения запросов и обработки ошибок.
/// Содержит базовый `URL` адресс.
class BaseRequestFactory: AbstractRequestFactory {
	
	// MARK: - Public properties
	let errorHandler: AbstractErrorHandler
	let errorParser: AbstractErrorParser
	let sessionManager: SessionManager
	let queue: DispatchQueue?
	let baseUrl = URL(string: "http://0.0.0.0:8181/")!
	
	// MARK: - Init
	/// Создает экземпляр объекта с нужными `errorHandler`, `errorParser`, `sessionManager`, `queue`.
	///
	/// - Parameters:
	///   - errorHandler: обработчик ошибок.
	///   - errorParser: парсер ошибок.
	///   - sessionManager: менеджер сессии (`Alamofire`).
	///   - queue: очередь выполнения запроса. По умолчанию `global`.
	init(
		errorHandler: AbstractErrorHandler,
		errorParser: AbstractErrorParser,
		sessionManager: SessionManager,
		queue: DispatchQueue? = DispatchQueue.global()) {
		
		self.errorHandler = errorHandler
		self.errorParser = errorParser
		self.sessionManager = sessionManager
		self.queue = queue
	}
	
	// MARK: - Public methods
	/// Отвечате за выполнение запросов в сеть с помощью `Alamofire` и парсинг полученного результата.
	///
	/// - Parameters:
	///   - request: путь на выполнение запроса
	///   - completionHandler: замыкание, вызываемое когда запрос выполнен
	/// - Returns: новый экземпляр `DataRequest`
	@discardableResult
	final public func request<T: Decodable>(
		request: URLRequestConvertible,
		completionHandler: @escaping (T) -> Void)
		-> DataRequest {
			return sessionManager
				.request(request)
				.responseCodable(errorParser: errorParser, queue: queue) { [weak self] (response: DataResponse<T>) in

					// Логика обработки результата парсинга
					switch response.result {
					case .success(let value):
						completionHandler(value)
					case .failure(let error):
						guard let customError = error as? AppError else {
							handleError("Необработанная ошибка")
							self?.errorHandler.handle(error: AppError.undefinedError(error.localizedDescription))
							return
						}
						self?.errorHandler.handle(error: customError)
					}
			}
	}
	
}
