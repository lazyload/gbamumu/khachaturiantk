//
//  AbstractRequestFactory.swift
//  GBShop
//
//  Created by Тигран on 29/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Alamofire

/// Абстракция фабрики для запросов в сеть.
/// Позволяет выполнять запросы в сеть и парсить полученные данные с помощью `Codable`.
protocol AbstractRequestFactory {
	var errorParser: AbstractErrorParser { get }
	var sessionManager: SessionManager { get }
	var queue: DispatchQueue? { get }
	
	/// Позволяет выполнять запросы в сеть с помощью `Alamofire`.
	///
	/// - Parameters:
	///   - request: путь на выполнение запроса.
	///   - completionHandler: замыкание, вызываемое когда запрос выполнен.
	/// - Returns: новый экземпляр `DataRequest`.
	@discardableResult
	func request<T: Decodable>(request: URLRequestConvertible,
							   completionHandler: @escaping (DataResponse<T>) -> Void) -> DataRequest
}

extension AbstractRequestFactory {
	
	@discardableResult
	public func request<T: Decodable>(request: URLRequestConvertible,
									  completionHandler: @escaping (DataResponse<T>) -> Void) -> DataRequest {
			return sessionManager.request(request).responseCodable(errorParser: errorParser,
																   queue: queue,
																   completionHandler: completionHandler)
	}
}
