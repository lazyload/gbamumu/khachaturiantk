//
//  BasketRequestFactory.swift
//  GBShop
//
//  Created by Тигран on 14/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Alamofire

/// Отвечает за выполнение запросов на работу с корзиной.
protocol BasketRequestFactory {
	
	/// Отвечает за добавление товара в корзину.
	///
	/// - Parameters:
	///   - id: идентификатор продукта.
	///   - quantity: количество продукта.
	///   - completion: замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func addProduct(id: Int, quantity: Int, completion: @escaping (SimpleResult) -> Void)
	
	/// Отвечает за удаление товара из корзины.
	///
	/// - Parameters:
	///   - id: идентификатор продукта.
	///   - completion: замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func deleteProduct(id: Int, completion: @escaping (SimpleResult) -> Void)
	
	/// Отвечает за получение текущей корзины пользователя.
	///
	/// - Parameters:
	///   - user: объект, содержащий информацию о пользователе.
	///   - completion: замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func getBasket(user: UserProtocol, completion: @escaping (BasketResult) -> Void)
	
	/// Отвечает за оплату.
	///
	/// - Parameters:
	///   - creditCardNumber: номер карты для оплаты заказа.
	///   - completion: замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func pay(creditCardNumber: String, completion: @escaping (MessageResult) -> Void)
	
}

final class BasketRequestFactoryImp: BaseRequestFactory, BasketRequestFactory {
	
	// MARK: - Public functions
	func addProduct(id: Int, quantity: Int, completion: @escaping (SimpleResult) -> Void) {
		let addRoute = AddProductRouter(baseUrl: baseUrl, id: id, quantity: quantity)
		request(request: addRoute, completionHandler: completion)
	}
	
	func deleteProduct(id: Int, completion: @escaping (SimpleResult) -> Void) {
		let deleteRoute = DeleteProductRouter(baseUrl: baseUrl, id: id)
		request(request: deleteRoute, completionHandler: completion)
	}
	
	func getBasket(user: UserProtocol, completion: @escaping (BasketResult) -> Void) {
		let getBasketRoute = GetBasketRouter(baseUrl: baseUrl)
		request(request: getBasketRoute, completionHandler: completion)
		
	}
	
	func pay(creditCardNumber: String, completion: @escaping (MessageResult) -> Void) {
		let payRoute = PayRouter(baseUrl: baseUrl, creditCardNumber: creditCardNumber)
		request(request: payRoute, completionHandler: completion)
	}
	
	// MARK: - Ptivate structs
	private struct AddProductRouter: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .post
		let path: String = "addToBasket"
		let encoding: RequestRouterEncoding = .json
		
		let id: Int
		let quantity: Int
		var parameters: Parameters? {
			return [
				"id_product": id,
				"quantity": quantity
			]
		}
		
	}
	
	private struct DeleteProductRouter: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .delete
		let path: String = "deleteFromBasket"
		let encoding: RequestRouterEncoding = .json
		
		let id: Int
		var parameters: Parameters? {
			return [
				"id_product": id
			]
		}
		
	}
	
	private struct GetBasketRouter: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .get
		let path: String = "getBasket"
		let parameters: Parameters? = nil
		
	}
	
	private struct PayRouter: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .post
		let path: String = "pay"
		let encoding: RequestRouterEncoding = .json
		
		let creditCardNumber: String
		var parameters: Parameters? {
			return [
				"creditCard": creditCardNumber
			]
		}
		
	}
}
