//
//  ReviewRequestFactory.swift
//  GBShop
//
//  Created by Тигран on 08/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Alamofire

/// Отвечает за выполнение запросов на добавление нового отзыва, удаление старого и публикацию отзыва.
protocol ReviewRequestFactory {
	
	/// Отвечает за добавление нового отзыва и отправку его на модерацию.
	///
	/// - Parameters:
	///   - user: объект, содержащий информацию о пользователе.
	///   - text: текст отзыва.
	///   - completion:  замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func addReview(user: UserProtocol, text: String, completion: @escaping (MessageResult) -> Void)
	
	/// Отвечает за удаление конкретного отзыва.
	///
	/// - Parameters:
	///   - idComment: идентификатор комментария.
	///   - completion:  замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func deleteReview(idComment: Int, completion: @escaping (SimpleResult) -> Void)
	
	/// Отвечает за принятие и публикацию отзыва. Требуются права администратора.
	///
	/// - Parameters:
	///   - idComment: идентификатор комментария.
	///   - user: объект, содержащий информацию о пользователе.
	///   - completion:  замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func approveReview(idComment: Int, user: UserProtocol, completion: @escaping (SimpleResult) -> Void)
	
}

final class ReviewRequestFactoryImp: BaseRequestFactory, ReviewRequestFactory {
	
	// MARK: - Public functions
	func addReview(user: UserProtocol, text: String, completion: @escaping (MessageResult) -> Void) {
		let addReviewRoute = AddReviewRoute(baseUrl: baseUrl, user: user, text: text)
		request(request: addReviewRoute, completionHandler: completion)
	}
	
	func deleteReview(idComment: Int, completion: @escaping (SimpleResult) -> Void) {
		let deleteReviewRoute = DeleteReviewRouter(baseUrl: baseUrl, id: idComment)
		request(request: deleteReviewRoute, completionHandler: completion)
	}
	
	func approveReview(idComment: Int, user: UserProtocol, completion: @escaping (SimpleResult) -> Void) {
		let approveReviewRoute = ApproveReviewRouter(baseUrl: baseUrl, id: idComment, user: user)
		request(request: approveReviewRoute, completionHandler: completion)
	}
	
	// MARK: - Ptivate structs
	private struct AddReviewRoute: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .post
		let path: String = "/addReview"
		let encoding: RequestRouterEncoding = .json
		
		let user: UserProtocol
		let text: String
		var parameters: Parameters? {
			return [
				"id_user" : user.userId,
				"text": text
			]
		}
		
	}
	
	private struct DeleteReviewRouter: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .delete
		let path: String = "/deleteReview"
		let encoding: RequestRouterEncoding = .json
		
		let id: Int
		var parameters: Parameters? {
			return [
				"id_comment" : id
			]
		}
		
	}
	
	private struct ApproveReviewRouter: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .post
		let path: String = "/approveReview"
		let encoding: RequestRouterEncoding = .json
		
		let id: Int
		let user: UserProtocol
		var parameters: Parameters? {
			return [
				"id_comment": id,
				"is_admin": user.isAdmin
			]
		}
		
	}
	
}
