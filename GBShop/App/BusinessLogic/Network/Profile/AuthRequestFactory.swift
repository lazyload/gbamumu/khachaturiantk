//
//  AuthRequestFactory.swift
//  GBShop
//
//  Created by Тигран on 29/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Alamofire

/// Отвечает за осуществление запросов авторизации пользователя.
/// Может производить запросы на вход, выход, регистрацию.
protocol AuthRequestFactory {
	
	/// Отвечает за выполнение входа пользователя в систему по логину и паролю.
	///
	/// - Parameters:
	///   - userName: имя пользователя.
	///   - password: пароль.
	///   - completionHandler: замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func login(userName: String, password: String, completionHandler: @escaping (LoginResult) -> Void)
	
	/// Отвечает за выход пользователя из системы.
	///
	/// - Parameters:
	///   - userId: `id` пользователя.
	///   - completionHandler: замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func logout(userId: Int, completionHandler: @escaping (SimpleResult) -> Void)
	
	/// Отвечате за регистрацию нового пользователя в системе.
	///
	/// - Parameters:
	///   - user: объект, содержащий информацию о новом пользователе.
	///   - completionHandler: замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func register(user: UserProtocol, completionHandler: @escaping (MessageResult) -> Void)
	
}

final class AuthRequestFactoryImp: BaseRequestFactory, AuthRequestFactory {
	
	// MARK: - Public functions
	func login(userName: String, password: String, completionHandler: @escaping (LoginResult) -> Void) {
		let loginRoute = LoginRouter(baseUrl: baseUrl, login: userName, password: password)
		request(request: loginRoute, completionHandler: completionHandler)
	}
	
	func logout(userId: Int, completionHandler: @escaping (SimpleResult) -> Void) {
		let logoutRoute = LogoutRouter(baseUrl: baseUrl, userId: userId)
		request(request: logoutRoute, completionHandler: completionHandler)
	}
	
	func register(user: UserProtocol, completionHandler: @escaping (MessageResult) -> Void) {
		let registerRoute = RegisterRouter(baseUrl: baseUrl, user: user)
		request(request: registerRoute, completionHandler: completionHandler)
	}
	
	// MARK: - Ptivate structs
	private struct LoginRouter: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .post
		let path: String = "login"
		let encoding: RequestRouterEncoding = .json
		
		let login: String
		let password: String
		var parameters: Parameters? {
			return [
				"username": login,
				"password": password
			]
		}
		
	}
	
	private struct LogoutRouter: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .post
		let path: String = "logout"
		let encoding: RequestRouterEncoding = .json
		
		let userId: Int
		var parameters: Parameters? {
			return ["id_user": userId]
		}
		
	}
	
	private struct RegisterRouter: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .post
		let path: String = "register"
		let encoding: RequestRouterEncoding = .json
		
		let user: UserProtocol
		var parameters: Parameters? {
			return [
				"id_user": user.userId,
				"username": user.username,
				"password": user.password,
				"email": user.email,
				"gender": user.gender ?? "",
				"credit_card": user.creditCard ?? "",
				"bio": user.bio ?? ""
			]
		}
		
	}
	
}

