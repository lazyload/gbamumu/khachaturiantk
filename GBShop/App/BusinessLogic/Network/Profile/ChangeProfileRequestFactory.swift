//
//  ChangeProfileRequestFactory.swift
//  GBShop
//
//  Created by Тигран on 30/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Alamofire

/// Отвечает за осуществление запросов на изменения пользовательских данных.
protocol ChangeProfileRequestFactory {
	
	/// Отвечает за изменение данных о пользователе
	///
	/// - Parameters:
	///   - user: объект, содержащий новую информацию о пользователе.
	///   - completionHandler: замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func change(user: UserProtocol, completionHandler: @escaping (SimpleResult) -> Void)
	
}

final class ChangeProfileRequestFactoryImp: BaseRequestFactory, ChangeProfileRequestFactory {
	
	// MARK: - Public functions
	func change(user: UserProtocol, completionHandler: @escaping (SimpleResult) -> Void) {
		let changeRoute = ChangeRouter(baseUrl: baseUrl, user: user)
		request(request: changeRoute, completionHandler: completionHandler)
	}
	
	// MARK: - Ptivate structs
	private struct ChangeRouter: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .post
		let path: String = "changeUserData"
		let encoding: RequestRouterEncoding = .json
		
		let user: UserProtocol
		var parameters: Parameters? {
			var params = [
				"id_user" : user.userId,
				"username" : user.username,
				"password" : user.password,
				"email" : user.email
				] as [String : Any]
			
			if let gender = user.gender {
				params["gender"] = gender
			}
			
			if let creditCard = user.creditCard {
				params["credit_card"] = creditCard
			}
			
			if let bio = user.bio {
				params["bio"] = bio
			}
			
			return params
		}
		
	}
	
}
