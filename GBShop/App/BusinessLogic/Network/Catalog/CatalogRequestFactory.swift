//
//  CatalogRequestFactory.swift
//  GBShop
//
//  Created by Тигран on 01/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Alamofire

/// Отвечает за осуществление запросов для выгрузки каталога и конкретного товара по `id`.
protocol CatalogRequestFactory {
	
	/// Отвечает за получения всего каталога.
	///
	/// - Parameter completionHandler: замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func getProducts(completionHandler: @escaping ([CatalogResult]) -> Void)
	
	/// Отвечает за получение продукта по id.
	///
	/// - Parameters:
	///   - id: идентификатор товара.
	///   - completionHandler: замыкание, вызываемое по окончанию запроса. Возвращает результат операции.
	func getProductBy(id: Int, completionHandler: @escaping (ProductResult) -> Void)
	
}

final class CatalogRequestFactoryImp: BaseRequestFactory, CatalogRequestFactory {
	
	// MARK: - Public functions
	func getProducts(completionHandler: @escaping ([CatalogResult]) -> Void) {
		let catalogRoute = GetCatalogRouter(baseUrl: baseUrl)
		request(request: catalogRoute, completionHandler: completionHandler)
	}
	
	func getProductBy(id: Int, completionHandler: @escaping (ProductResult) -> Void) {
		let productRoute = GetProductRouter(baseUrl: baseUrl, id: id)
		request(request: productRoute, completionHandler: completionHandler)
	}
	
	// MARK: - Ptivate structs
	private struct GetCatalogRouter: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .get
		let path: String = "getCatalogue"
		let parameters: Parameters? = nil
		
	}
	
	private struct GetProductRouter: RequestRouter {
		
		let baseUrl: URL
		let method: HTTPMethod = .get
		let path: String = "getProduct"
		
		let id: Int
		var parameters: Parameters? {
			return [
				"id_product": id
			]
		}
		
	}
	
}
