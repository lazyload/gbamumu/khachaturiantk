//
//  AppDelegate.swift
//  GBShop
//
//  Created by Тигран on 24/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit
import YandexMobileMetrica

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	// MARK: - Public properties
	var window: UIWindow?
	var yandexApiKey = "a4ea2528-c1d0-4b63-90cc-1782425b4c14"
	
	// MARK: - Public methods
	func application(_ application: UIApplication,
					 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		
		if let yandexConfig = YMMYandexMetricaConfiguration(apiKey: yandexApiKey) {
			YMMYandexMetrica.activate(with: yandexConfig)
		} else {
			handleError("Не получилось создать конфигурацию AppMetrica")
		}
		
		setupNavigationBar()
		return true
	}
	
	// MARK: - Private methods
	private func setupNavigationBar() {
		let navBarAppearance = UINavigationBar.appearance()
		navBarAppearance.largeTitleTextAttributes = [
			NSAttributedString.Key.foregroundColor: UIColor.darkTextColor,
			NSAttributedString.Key.font: UIFont.init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Heavy", size: 40), size: 40)
		]
		navBarAppearance.titleTextAttributes = [
			NSAttributedString.Key.foregroundColor: UIColor.darkTextColor,
			NSAttributedString.Key.font: UIFont.init(descriptor: UIFontDescriptor(name: "SFUIDisplay-Heavy", size: 18), size: 18)
		]
		
		navBarAppearance.barTintColor = UIColor.brandBlue
		navBarAppearance.isTranslucent = true
		navBarAppearance.setBackgroundImage(UIImage(named: "NavBarBackground"), for: .default)
		navBarAppearance.shadowImage = UIImage()
	}
	
}

func handleError(_ error: String) {
	#if DEBUG
	Swift.assertionFailure(error)
	#else
	YMMYandexMetrica.reportError(error, exception: nil, onFailure: nil)
	#endif
}
