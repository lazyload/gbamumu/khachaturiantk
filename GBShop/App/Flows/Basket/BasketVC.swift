//
//  BasketVC.swift
//  GBShop
//
//  Created by Тигран on 26/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

final class BasketVC: UIViewController, TrackableMixin {

	// MARK: - Dependency
	private lazy var basketFactory: BasketRequestFactory! = {
		return di.resolve(BasketRequestFactory.self)
	}()
	
	private lazy var converter: NumberConverter! = {
		return di.resolve(NumberConverter.self)
	}()
	
	private lazy var viewDecorator: ViewDecorator! = {
		return di.resolve(ViewDecorator.self)
	}()
	
	private lazy var alertCenter: AlertCenter! = {
		return di.resolve(AlertCenter.self)
	}()
	
	private lazy var userRepository: UserRepository? = {
		return di.resolve(UserRepository.self)
	}()
	
	// MARK: - IBOutlet
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var basketFooter: BasketFooter! {
		didSet {
			viewDecorator.applySmoothCorners(view: basketFooter)
			viewDecorator.applyShadow(view: basketFooter,
									  size: .medium,
									  color: .black,
									  place: .top)
		}
	}
	
	@IBOutlet weak var emptyLabel: UILabel!
	@IBOutlet weak var paymentView: PaymentView! {
		didSet {
			viewDecorator.applySmoothCorners(view: paymentView)
		}
	}
	
	@IBOutlet weak var dimView: UIView!
	
	// MARK: - Private properties
	private var basket: BasketResult?
	private lazy var user: UserProtocol? = {
		return userRepository?.load()
	}()
	
	// MARK: - ViewController lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.register(UINib(nibName: BasketCell.identifier, bundle: nil),
						   forCellReuseIdentifier: BasketCell.identifier)
		tableView.delegate = self
		tableView.dataSource = self
		
		basketFooter.delegate = self
		
		paymentView.delegate = self
		
		guard let user = user else {
			handleError("User not found")
			return
		}
		
		basketFactory.getBasket(user: user) { [weak self] response in
			self?.basket = response
			DispatchQueue.main.async {
				self?.setupFooter()
				self?.tableView.reloadData()
				self?.showFooter()
			}
		}
		
		let hidePayViewGesture = UITapGestureRecognizer(target: self, action: #selector(hidePaymentView))
		hidePayViewGesture.cancelsTouchesInView = false
		dimView.addGestureRecognizer(hidePayViewGesture)
		
		emptyLabel.isHidden = false
	}
	
	// MARK: - Private methods
	private func setupFooter() {
		basketFooter.count = " \(basket?.countGoods.description ?? "") "
		basketFooter.price = converter.getPriceInRubles(basket?.amount) ?? "-"
	}
	
	private func showFooter() {
		UIView.animate(withDuration: 0.5) {
			self.basketFooter.transform = CGAffineTransform.init(translationX: 0,
																 y: -160)
			self.basketFooter.isShowed = true
			self.emptyLabel.isHidden = true
		}
	}
	
	private func hideFooter() {
		guard basketFooter.isShowed else { return }
		UIView.animate(withDuration: 0.5) {
			self.basketFooter.transform = CGAffineTransform.init(translationX: 0,
																 y: 160)
			self.basketFooter.isShowed = false
		}
	}
	
	private func showPaymentView() {
		self.navigationController?.navigationBar.layer.zPosition = -1
		UIView.animate(withDuration: 0.5) {
			self.dimView.alpha = 0.8
			self.paymentView.transform = CGAffineTransform.init(translationX: 0,
																y: -380)
		}
	}
	
	@objc private func hidePaymentView() {
		UIView.animate(withDuration: 0.5, animations: {
			self.dimView.alpha = 0.0
			self.paymentView.transform = CGAffineTransform.init(translationX: 0,
																y: 380)
		}, completion: { _ in
			self.navigationController?.navigationBar.layer.zPosition = 0
		})
	}
	
}

extension BasketVC: UITableViewDataSource {
	
	// MARK: - UITableViewDataSource
	func tableView(_ tableView: UITableView,
				   numberOfRowsInSection section: Int) -> Int {
		return basket?.countGoods ?? 0
	}
	
	func tableView(_ tableView: UITableView,
				   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if let cell = tableView.dequeueReusableCell(withIdentifier: BasketCell.identifier,
													for: indexPath) as? BasketCell {
			if let product = basket?.contents?[indexPath.row] {
				cell.productImage = UIImage(named: product.name)
				cell.count = "\(product.quantity) шт."
				cell.name = product.name
				cell.price = converter.getPriceInRubles(product.price) ?? ""
			}
			return cell
		}
		assertionFailure("BasketCell не найдена")
		return UITableViewCell()
	}
	
}

extension BasketVC: UITableViewDelegate {
	
	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView,
				   commit editingStyle: UITableViewCell.EditingStyle,
				   forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			if let product = basket?.contents?[indexPath.row] {
				basketFactory.deleteProduct(id: product.id) { [weak self] _ in
					self?.track(.deletedFromBasket(id: product.id))
					self?.basket?.countGoods -= self?.basket?.contents?[indexPath.row].quantity ?? 0
					self?.basket?.amount -= ((self?.basket?.contents?[indexPath.row].quantity ?? 0) *
											 (self?.basket?.contents?[indexPath.row].price ?? 0))
					self?.basket?.contents?.remove(at: indexPath.row)
					DispatchQueue.main.async {
						self?.tableView.reloadData()
						self?.setupFooter()
					}
				}
			}
			assertionFailure("Не найден удаляемый продукт")
		}
	}
	
}

extension BasketVC: BasketFooterDelegate {
	
	// MARK: - BasketFooterDelegate
	func checkoutPressed() {
		showPaymentView()
	}
	
}

extension BasketVC: PaymentProtocol {
	
	// MARK: - PaymentProtocol
	func didPressPay() {
		guard let user = user else {
			handleError("User not found")
			return
		}
		
		guard let creditCard = user.creditCard else {
			alertCenter.showErrorAlert(withTitle: nil,
									   message: "Пожалуйста, сначала введите номер карты в профиле.",
									   handler: nil)
			return
		}
		basketFactory.pay(creditCardNumber: creditCard) { [weak self] response in
			self?.track(.orderBeenPayed())
			self?.alertCenter.showNotificationAlert(withTitle: nil,
													message: response.message,
													handler: { _ in
														self?.hidePaymentView()
														self?.basket = nil
														DispatchQueue.main.async {
															self?.tableView.reloadData()
															self?.emptyLabel.isHidden = false
															self?.hideFooter()
														}
			})
		}
	}
	
}
