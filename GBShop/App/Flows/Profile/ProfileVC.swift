//
//  ProfileVC.swift
//  GBShop
//
//  Created by Тигран on 19/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

/// Отвечает за показ экрана профиля пользователя.
final class ProfileVC: UIViewController {
	
	// MARK: - Dependency
	private lazy var changeRequestFactory: ChangeProfileRequestFactory? = {
		return di.resolve(ChangeProfileRequestFactory.self)
	}()
	
	private lazy var alertCenter: AlertCenter? = {
		return di.resolve(AlertCenter.self)
	}()
	
	private lazy var viewDecorator: ViewDecorator? = {
		return di.resolve(ViewDecorator.self)
	}()
	
	private lazy var userRepository: UserRepository? = {
		return di.resolve(UserRepository.self)
	}()

	// MARK: - IBOutlet
	@IBOutlet weak var userImage: RoundImageView!
	@IBOutlet weak var userName: UILabel!
	@IBOutlet weak var emailCell: ProfileCell!
	@IBOutlet weak var bioCell: ProfileCell!
	@IBOutlet weak var creditCardCell: ProfileCell!
	@IBOutlet weak var oldPasswordCell: ProfileCell!
	@IBOutlet weak var newPasswordCell: ProfileCell!
	@IBOutlet weak var conformPasswordCell: ProfileCell!
	
	// MARK: - Private properties
	private lazy var user: UserProtocol? = {
		return userRepository?.load()
	}()
	
	// MARK: - ViewController lifecycle
	override func viewDidLoad() {
		
		setupCells()
	}
	
	// MARK: - IBAction
	@IBAction func savePressed(_ sender: Any) {
		guard var user = user else {
			handleError("User not found")
			return
		}
		
		guard !emailCell.value.isEmpty else {
			alertCenter?.showErrorAlert(withTitle: nil,
										message: "Пожалуйста, заполните адрес электронной почты.",
										handler: nil)
			return
		}

		user.email = emailCell.value
		user.bio = bioCell.value
		user.creditCard = creditCardCell.value

		if !newPasswordCell.value.isEmpty {
			guard !oldPasswordCell.value.isEmpty,
				checkOldPassword(password: oldPasswordCell.value) else {
					alertCenter?.showErrorAlert(withTitle: nil,
												message: "Неправильный пароль.",
												handler: nil)
					return
			}

			guard checkNewPassword(password: newPasswordCell.value) else {
				alertCenter?.showErrorAlert(withTitle: nil,
											message: "Введеные пароли не совпадают.",
											handler: nil)
				return
			}

			user.password = newPasswordCell.value
		}

		changeRequestFactory?.change(user: user, completionHandler: { [weak self] response in
			if response.result == 1 {
				DispatchQueue.main.async {
					self?.alertCenter?.showNotificationAlert(withTitle: nil,
															 message: "Данные успешно изменены.",
															 handler: nil)
				}
			}
		})
	}
	
	// MARK: - Private methods
	private func setupCells() {
		guard let user = user else {
			handleError("User not found")
			return
		}
		userName.text = "\(user.firstname ?? "") \(user.lastname ?? "")"
		emailCell.value = user.email
		bioCell.value = user.bio ?? ""
		creditCardCell.value = user.creditCard ?? ""
	}
	
	private func checkOldPassword(password: String) -> Bool {
		guard let user = user else {
			handleError("User not found")
			return false
		}
		return password == user.password
	}
	
	private func checkNewPassword(password: String) -> Bool {
		return  conformPasswordCell.value == password
	}
	
}
