//
//  RegisterVC.swift
//  GBShop
//
//  Created by Тигран on 15/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

 /// Отвечает за показ экрана регистрации нового пользователя.
 final class RegisterVC: ScrollableVC, TrackableMixin {
	
	// MARK: - Dependency
	private lazy var authRequestFactory: AuthRequestFactory? = {
		return di.resolve(AuthRequestFactory.self)
	}()
	
	private lazy var alertCenter: AlertCenter? = {
		return di.resolve(AlertCenter.self)
	}()
	
	private lazy var userRepository: UserRepository? = {
		return di.resolve(UserRepository.self)
	}()
	
	// MARK: - IBOutlet
	@IBOutlet weak private var usernameField: TextFieldView!
	@IBOutlet weak private var emailField: TextFieldView!
	@IBOutlet weak private var passwordField: TextFieldView!
	@IBOutlet weak private var registerButton: RoundButtonWithShadow!
	
	// MARK: - ViewController lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	// MARK: - IBAction
	@IBAction func registerPressed(_ sender: Any) {
		let username = usernameField.text
		let email = emailField.text
		let password = passwordField.text
		
		guard !username.isEmpty, !email.isEmpty, !password.isEmpty else {
				alertCenter?.showErrorAlert(withTitle: nil,
											 message: "Пожалуйста, заполните все поля.",
											 handler: nil)
				return
		}
		
		let user = User(userId: 123,
						username: username,
						password: password,
						email: email)
		
		authRequestFactory?.register(user: user, completionHandler: { [weak self] response in
			if response.result == 1 {
				self?.track(.registration(username: username, email: email))
				self?.userRepository?.save(user)
				DispatchQueue.main.async {
					self?.alertCenter?.showNotificationAlert(withTitle: nil, message: "Вы успешно зарегистрированы!", handler: { _ in
						self?.track(.login(login: username, success: true))
						self?.performSegue(withIdentifier: "login", sender: self)
					})
				}
			}
		})
	}
	
}
