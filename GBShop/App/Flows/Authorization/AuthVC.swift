//
//  AuthVC.swift
//  GBShop
//
//  Created by Тигран on 15/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

/// Отвечает за показ экрана авторизации пользователя.
final class AuthVC: UIViewController, TrackableMixin {
	
	// MARK: - IBOutlet
	@IBOutlet weak private var registerButton: RoundButtonWithShadow!
	
	// MARK: - ViewController lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.navigationController?.navigationBar.isHidden = true
	}
	
	// MARK: - IBAction
	@IBAction func loginPressed(_ sender: Any) {
		performSegue(withIdentifier: "loginUser", sender: self)
	}
	
	@IBAction func registerPressed(_ sender: Any) {
		performSegue(withIdentifier: "registerUser", sender: self)
	}
	
	@IBAction func unwindToAuthVC(segue:UIStoryboardSegue) {
		track(.logout)
	}
	
}
