//
//  ScrollableVC.swift
//  GBShop
//
//  Created by Тигран on 22/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

class ScrollableVC: UIViewController {
	
	// MARK: - IBOutlet
	@IBOutlet weak private var scrollView: UIScrollView!

	// MARK: - ViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
		
		let hideKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
		hideKeyboardGesture.cancelsTouchesInView = false
		view.addGestureRecognizer(hideKeyboardGesture)
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self,
											   selector: #selector(self.keyboardWasShown(notification:)),
											   name: UIResponder.keyboardWillShowNotification,
											   object: nil)
		
		NotificationCenter.default.addObserver(self,
											   selector: #selector(self.keyboardWillBeHidden(notification:)),
											   name: UIResponder.keyboardWillHideNotification,
											   object: nil)
	}
	
	override public func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
	}
	
	// MARK: Private methods
	@objc private func keyboardWasShown(notification: Notification) {
		let info = notification.userInfo! as NSDictionary
		// swiftlint:disable force_cast
		let kbSize = (info.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue).cgRectValue.size
		// swiftlint:enable force_cast
		let contentInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0)
		
		scrollView?.contentInset = contentInsets
		scrollView?.scrollIndicatorInsets = contentInsets
		
		scrollView.isScrollEnabled = true
	}
	
	@objc private func keyboardWillBeHidden(notification: Notification) {
		let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		scrollView?.contentInset = contentInsets
		scrollView?.scrollIndicatorInsets = contentInsets
		
		scrollView.bounds = CGRect(x: 0.0, y: 0.0, width: scrollView.bounds.width, height: scrollView.bounds.height)
		
		scrollView.isScrollEnabled = false
	}
	
	@objc private func hideKeyboard() {
		scrollView.endEditing(true)
		
		scrollView.bounds = CGRect(x: 0.0, y: 0.0, width: scrollView.bounds.width, height: scrollView.bounds.height)
		
		scrollView.isScrollEnabled = false
	}

}
