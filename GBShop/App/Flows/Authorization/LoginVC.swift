//
//  LoginVC.swift
//  GBShop
//
//  Created by Тигран on 22/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

final class LoginVC: ScrollableVC, TrackableMixin {
	
	// MARK: - Dependency
	private lazy var authRequestFactory: AuthRequestFactory? = {
		return di.resolve(AuthRequestFactory.self)
	}()
	
	private lazy var alertCenter: AlertCenter? = {
		return di.resolve(AlertCenter.self)
	}()
	
	private lazy var userRepository: UserRepository? = {
		return di.resolve(UserRepository.self)
	}()

	// MARK: - IBOutlet
	@IBOutlet weak private var usernameField: TextFieldView!
	@IBOutlet weak private var passwordField: TextFieldView!
	@IBOutlet weak private var loginButton: RoundButtonWithShadow!
	
	// MARK: - Private properties
	private var user: UserProtocol?
	
	// MARK: - ViewController lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	// MARK: - IBAction
	@IBAction func loginPressed(_ sender: Any) {
		let username = usernameField.text
		let password = passwordField.text
		
		guard !username.isEmpty, !password.isEmpty else {
			alertCenter?.showErrorAlert(withTitle: nil,
										message: "Заполните оба поля",
										handler: nil)
			return
		}
		
		authRequestFactory?.login(userName: username,
								  password: password,
								  completionHandler: { [weak self] response in
									let user = User(userId: response.user.id,
															username: username,
															password: password,
															email: response.user.email,
															isAdmin: response.user.isAdmin,
															firstname: response.user.firstname,
															lastname: response.user.lastname,
															gender: response.user.gender,
															creditCard: response.user.creditCard,
															bio: response.user.bio)
									self?.userRepository?.save(user)
									
									self?.track(.login(login: username,
													   success: true))
									
									DispatchQueue.main.async {
										self?.performSegue(withIdentifier: "login", sender: self)
									}
		})
	}
	
	@IBAction func forgetPasswordPressed(_ sender: Any) {
		alertCenter?.showNotificationAlert(withTitle: "Подсказочка",
										   message: """
														Имя пользователя: username
														Пароль: 123456
													""",
										   handler: nil)
	}

}
