//
//  ProductVC.swift
//  GBShop
//
//  Created by Тигран on 22/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

final class ProductVC: UIViewController, TrackableMixin {

	// MARK: - Dependency
	private lazy var catalogFactory: CatalogRequestFactory! = {
		return di.resolve(CatalogRequestFactory.self)
	}()
	
	private lazy var viewDecorator: ViewDecorator! = {
		return di.resolve(ViewDecorator.self)
	}()

	private lazy var converter: NumberConverter! = {
		return di.resolve(NumberConverter.self)
	}()
	
	private lazy var alertCenter: AlertCenter! = {
		return di.resolve(AlertCenter.self)
	}()
	
	private lazy var basketFactory: BasketRequestFactory! = {
		return di.resolve(BasketRequestFactory.self)
	}()
	
	// MARK: - IBOutlet
	@IBOutlet weak private var productInfo: ProductInfoView! {
		didSet {
			viewDecorator.applySmoothCorners(view: productInfo)
			viewDecorator.applyShadow(view: productInfo,
									   size: .medium,
									   color: .black,
									   place: .bottom)
			productInfo.delegate = self
		}
	}
	
	@IBOutlet weak private var productDescription: ProductDescriptionView!
	@IBOutlet weak private var showCommentsButton: UIButton!
	
	// MARK: - Public properties
	var productId: Int!
	
	// MARK: - Private properties
	private var product: ProductResult? {
		didSet {
			DispatchQueue.main.async {
				self.setupProductViews()
			}
		}
	}
	
	// MARK: - ViewController lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
		getProductInfo()
		track(.productViewed(id: productId))
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		navigationController?.navigationBar.prefersLargeTitles = false
		hideTabBar()
	}
	
	// MARK: - IBAction
	@IBAction func showCommentsPressed(_ sender: Any) {
		if showCommentsButton.titleLabel?.text != "Комментариев пока нет" {
			performSegue(withIdentifier: "showComments", sender: self)
		}
	}
	
	// MARK: - Private methods
	private func getProductInfo() {
		catalogFactory.getProductBy(id: productId) { [weak self] response in
			self?.product = response
		}
	}
	
	private func setupProductViews() {
		guard let product = self.product else {
			handleError("Продукт не загружен")
			return
		}
		productInfo.image = UIImage(named: product.name)
		productInfo.title = product.name
		productInfo.price = converter.getPriceInRubles(product.price) ?? "Цена не доступна."
		productDescription.text = product.description
	}
	
}

extension ProductVC: ItemSelectedToBasket {
	
	// MARK: - ItemSelectedToBasket
	func itemSelected() {
		basketFactory.addProduct(id: productId, quantity: 1) { [weak self] _ in
			self?.track(.addedToBasket(id: self?.productId ?? 0))
			self?.alertCenter.showNotificationAlert(withTitle: nil,
											  message: "Товар успешно добавлен в корзину.",
											  handler: nil)
		}
	}
	
}
