//
//  CommentsVC.swift
//  GBShop
//
//  Created by Тигран on 24/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

final class CommentsVC: UIViewController, TrackableMixin {
	
	// MARK: - Dependency
	private lazy var viewDecorator: ViewDecorator? = {
		return di.resolve(ViewDecorator.self)
	}()
	
	private lazy var reviewFactory: ReviewRequestFactory? = {
		return di.resolve(ReviewRequestFactory.self)
	}()
	
	private lazy var userRepository: UserRepository? = {
		return di.resolve(UserRepository.self)
	}()
	
	private lazy var alertCenter: AlertCenter? = {
		return di.resolve(AlertCenter.self)
	}()
	
	// MARK: - IBOutlet
	@IBOutlet weak private var tableView: UITableView!
	@IBOutlet weak private var commentViewBottomConstraint: NSLayoutConstraint!
	@IBOutlet weak var newCommentView: NewCommentView!
	
	// MARK: - Private properties
	private let comments: [String] = ["Вау, какой круто ноутбук!!!❤️❤️❤️",
									  "У меня был такой. Ничего особенного.",
									  "А сколько в нем оперативной памяти?"]
	private lazy var user: UserProtocol? = {
		return userRepository?.load()
	}()
	
	// MARK: - ViewController lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.delegate = self
		tableView.dataSource = self
		tableView.register(UINib(nibName: CommentCell.identifier, bundle: nil),
						   forCellReuseIdentifier: CommentCell.identifier)
		
		newCommentView.delegate = self
		
		let hideKbGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
		view.addGestureRecognizer(hideKbGesture)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NotificationCenter.default.addObserver(self,
											   selector: #selector(self.keyboardWasShown(notification:)),
											   name: UIResponder.keyboardWillShowNotification,
											   object: nil)
		
		NotificationCenter.default.addObserver(self,
											   selector: #selector(self.keyboardWillBeHidden(notification:)),
											   name: UIResponder.keyboardWillHideNotification,
											   object: nil)
	}
	
	override public func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
	}
	
	// MARK: Private methods
	@objc private func keyboardWasShown(notification: Notification) {
		let info = notification.userInfo! as NSDictionary
		// swiftlint:disable force_cast
		let kbSize = (info.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue).cgRectValue.size
		// swiftlint:enable force_cast
		commentViewBottomConstraint.constant = kbSize.height - view.safeAreaInsets.bottom
	}
	
	@objc private func keyboardWillBeHidden(notification: Notification) {
		commentViewBottomConstraint.constant = 0
	}
	
	@objc private func hideKeyboard() {
		newCommentView.commentTextView.endEditing(true)
		commentViewBottomConstraint.constant = 0
	}
	
}

extension CommentsVC: UITableViewDelegate, UITableViewDataSource {
	
	// MARK: - UITableViewDataSource
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return comments.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if let cell = tableView.dequeueReusableCell(withIdentifier: CommentCell.identifier, for: indexPath) as? CommentCell {
			cell.comment = comments[indexPath.row]
			return cell
		}
		handleError("Не получилось получить CommentCell")
		return UITableViewCell()
	}
	
}

extension CommentsVC: NewCommentProtocol {
	
	// MARK: - NewCommentProtocol
	func commentSend(_ text: String) {
		guard let user = user else {
			handleError("User not found")
			return
		}
		
		guard !text.isEmpty else { return }
		
		newCommentView.commentTextView.resignFirstResponder()
		
		reviewFactory?.addReview(user: user, text: text, completion: { [weak self] response in
			self?.track(.commentAdded(text: text, username: user.username))
			self?.alertCenter?.showNotificationAlert(withTitle: nil,
													 message: response.message, handler: { _ in
														self?.newCommentView.commentTextView.text = ""
			})
		})
	}
	
}
