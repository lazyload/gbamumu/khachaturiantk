//
//  CatalogVC.swift
//  GBShop
//
//  Created by Тигран on 22/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

final class CatalogVC: UIViewController, TrackableMixin {
	
	// MARK: - Dependency
	private lazy var catalogFactory: CatalogRequestFactory? = {
		return di.resolve(CatalogRequestFactory.self)
	}()
	
	private lazy var alertCenter: AlertCenter? = {
		return di.resolve(AlertCenter.self)
	}()
	
	private lazy var viewDecorator: ViewDecorator? = {
		return di.resolve(ViewDecorator.self)
	}()
	
	private lazy var converter: NumberConverter? = {
		return di.resolve(NumberConverter.self)
	}()

	// MARK: - IBOutlet
	@IBOutlet weak private var collectionView: UICollectionView!
	
	// MARK: - Private properties
	private var products: [CatalogResult]?
	
	// MARK: - ViewController lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
		collectionViewSetup()
		loadCatalog()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		navigationController?.navigationBar.prefersLargeTitles = true
		showTabBar()
		track(.catalogViewed())
	}
	
	// MARK: - Private methods
	private func collectionViewSetup() {
		collectionView.delegate = self
		collectionView.dataSource = self
		collectionView.register(UINib(nibName: ProductCell.identifier, bundle: nil),
								forCellWithReuseIdentifier: ProductCell.identifier)
	}
	
	private func loadCatalog() {
		catalogFactory?.getProducts(completionHandler: { [weak self] response in
			self?.products = response
			for _ in 0...3 {
				self?.products?.append(contentsOf: response)
			}
			DispatchQueue.main.async {
				self?.collectionView.reloadData()
			}
		})
	}
	
	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showDetail" {
			guard let indexPath = collectionView.indexPathsForSelectedItems?[0],
				  let product = products?[indexPath.row],
				  let productVC = segue.destination as? ProductVC else {
				alertCenter?.showEnternalErrorAlert(handler: { _ in
					handleError("Невозможно осуществить переход на экран конкретного продукта")
				})
				return
			}
			
			productVC.productId = product.id
		}
	}
	
}

extension CatalogVC: UICollectionViewDelegate, UICollectionViewDataSource {
	
	// MARK: - UICollectionViewDataSource
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return products?.count ?? 0
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		 if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCell.identifier,
														  for: indexPath) as? ProductCell {
			guard let product = products?[indexPath.row] else {
				return UICollectionViewCell()
			}
			cell.image = UIImage(named: product.name)
			cell.name = product.name
			cell.price = converter?.getPriceInRubles(product.price) ?? "Цена не доступна."
			
			viewDecorator?.applySmoothCorners(view: cell)
			viewDecorator?.applyShadow(view: cell,
									   size: .small,
									   color: .black,
									   place: .bottom)
			
			return cell
		}
		handleError("Не получилось получить ProductCell")
		return UICollectionViewCell()
	}
	
	// MARK: - UICollectionViewDelegate
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
		UIView.animate(withDuration: 0.2,
					   animations: {
			collectionView.cellForItem(at: indexPath)?.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
		}, completion: { _ in
			UIView.animate(withDuration: 0.2) {
				collectionView.cellForItem(at: indexPath)?.transform = CGAffineTransform.identity
				self.performSegue(withIdentifier: "showDetail", sender: self)
			}
		})
	}
	
}
