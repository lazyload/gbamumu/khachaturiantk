//
//  SimpleResult.swift
//  GBShop
//
//  Created by Тигран on 30/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Класс для парсинга и хранения ответа с сервера. Содержит код выполненой операции - `result`:
/// 1 - успех;
/// 0 - ошибка.
final class SimpleResult: Codable {
	
	let result: Int
	
}
