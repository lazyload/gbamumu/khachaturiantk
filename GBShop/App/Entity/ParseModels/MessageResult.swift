//
//  MessageResult.swift
//  GBShop
//
//  Created by Тигран on 30/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Класс для парсинга и хранения ответа с сервера. Содержит код выполненой операции - `result`:
/// 1 - успех;
/// 0 - ошибка.
/// Также содержит сообщение о выполнении операции.
final class MessageResult: Codable {
	
	let result: Int
	let message: String
	
	enum CodingKeys: String, CodingKey {
		
		case result = "result"
		case message = "userMessage"
		
	}
	
}
