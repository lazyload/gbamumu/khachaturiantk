//
//  ProductResult.swift
//  GBShop
//
//  Created by Тигран on 01/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Класс для парсинга и хранения ответа с сервера. Содержит код выполненой операции - `result`:
/// 1 - успех;
/// 0 - ошибка.
/// Также содержит информацию о экземляре продукта из каталога.
final class ProductResult: Codable {
	
	var result: Int
	var name: String
	var price: Cents
	var description: String
	
	enum CodingKeys: String, CodingKey {
		
		case result = "result"
		case name = "product_name"
		case price = "product_price"
		case description = "product_description"
		
	}
	
}
