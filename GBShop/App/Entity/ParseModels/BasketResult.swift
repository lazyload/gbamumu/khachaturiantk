//
//  BasketResult.swift
//  GBShop
//
//  Created by Тигран on 14/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Класс для парсинга и хранения ответа с сервера. Содержит результат получения корзины клиента.
/// Содержит общую стоимость, кол-во товаров в корзине и список товаров.
final class BasketResult: Codable {
	
	var amount: Int
	var countGoods: Int
	var contents: [BasketProduct]?
	
}

/// Класс для парсинга и хранения ответа с сервера. Содержит результат получения товара в корзине.
final class BasketProduct: Codable {
	
	let id: Int
	let name: String
	let price: Cents
	let quantity: Int
	
	enum CodingKeys: String, CodingKey {
		case id = "id_product"
		case name = "product_name"
		case price = "price"
		case quantity = "quantity"
	}
	
}
