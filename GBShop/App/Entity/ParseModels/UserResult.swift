//
//  UserResult.swift
//  GBShop
//
//  Created by Тигран on 29/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Класс для парсинга и хранения ответа с сервера. Содержит информацию о пользователе.
final class UserResult: Codable {
	
	let id: Int
	let login: String
	var email: String
	let isAdmin: Bool
	var firstname: String?
	var lastname: String?
	let gender: String?
	var creditCard: String?
	var bio: String?
	
	enum CodingKeys: String, CodingKey {
		
		case id = "id_user"
		case login = "user_login"
		case firstname = "user_name"
		case lastname = "user_lastname"
		case email = "email"
		case isAdmin = "is_admin"
		case gender = "gender"
		case creditCard = "credit_card"
		case bio = "bio"
		
	}
	
}
