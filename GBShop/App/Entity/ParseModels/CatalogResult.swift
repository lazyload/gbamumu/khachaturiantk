//
//  CatalogResult.swift
//  GBShop
//
//  Created by Тигран on 01/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Класс для парсинга и хранения ответа с сервера. Содержит информацию о экземляре продукта из каталога.
final class CatalogResult: Codable {
	
	var id: Int
	var name: String
	var price: Cents
	
	enum CodingKeys: String, CodingKey {
		
		case id = "id_product"
		case name = "product_name"
		case price = "price"
		
	}
	
}
