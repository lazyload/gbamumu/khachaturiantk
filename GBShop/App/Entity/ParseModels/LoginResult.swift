//
//  LoginResult.swift
//  GBShop
//
//  Created by Тигран on 29/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Класс для парсинга и хранения ответа с сервера. Содержит код выполненой операции - `result`:
/// 1 - успех;
/// 0 - ошибка.
/// Также содержит экземпляр пользователя, в случае успеха.
final class LoginResult: Codable {
	
	let result: Int
	let user: UserResult
	
}
