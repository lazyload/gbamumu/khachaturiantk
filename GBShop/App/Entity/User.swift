//
//  User.swift
//  GBShop
//
//  Created by Тигран on 07/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

/// Абстракция пользователя.
protocol UserProtocol {
	
	var userId: Int { get }
	var username: String { get }
	var password: String { get set }
	var email: String { get set }
	var isAdmin: Bool { get }
	var firstname: String? { get set }
	var lastname: String? { get set }
	var gender: String? { get set }
	var creditCard: String? { get set }
	var bio: String? { get set }
	
}

/// Структура, хранящая сущность текущего пользователя.
struct User: UserProtocol {
	
	let userId: Int
	let username: String
	var password: String
	var email: String
	var isAdmin: Bool
	var firstname: String?
	var lastname: String?
	var gender: String?
	var creditCard: String?
	var bio: String?
	
	init(userId: Int,
		username: String,
		password: String,
		email: String,
		isAdmin: Bool = false,
		firstname: String? = nil,
		lastname: String? = nil,
		gender: String? = nil,
		creditCard: String? = nil,
		bio: String? = nil) {
		
		self.userId = userId
		self.username = username
		self.password = password
		self.email = email
		self.isAdmin = isAdmin
		self.firstname = firstname
		self.lastname = lastname
		self.gender = gender
		self.creditCard = creditCard
		self.bio = bio
	}
	
}
