//
//  HelpersAssembly.swift
//  GBShop
//
//  Created by Тигран on 15/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Swinject

/// Сборщик контейнера для работы хелперов приложения.
final class HelpersAssembly: Assembly {
	
	// MARK: - Public methods
	func assemble(container: Container) {
		container.register(AlertCenter.self) { _ in
			return AlertCenterImp()
		}
		
		container.register(ViewDecorator.self) { _ in
			return ViewDecoratorImp()
		}
		
		container.register(NumberConverter.self) { _ in
			let formatter = NumberFormatter()
			return NumberConverterImp(formatter)
		}
		
		container.register(UserRepository.self) { _ in
			return UserRepositoryImp()
		}.inObjectScope(.container)
	}
	
}
