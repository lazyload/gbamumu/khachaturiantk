//
//  NetworkAssembly.swift
//  GBShop
//
//  Created by Тигран on 29/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Alamofire
import Swinject

/// Сборщик контейнера для работы с сетью.
final class NetworkAssembly: Assembly {
	
	// MARK: - Public methods
	func assemble(container: Container) {
		container.register(AbstractErrorParser.self) { _ in
			return ErrorParser()
			}.inObjectScope(.container)
		
		container.register(AbstractErrorHandler.self) { resolver in
			return ShowAlertErrorHandler(alertCenter: resolver.resolve(AlertCenter.self)!)
			}.inObjectScope(.container)
		
		container.register(SessionManager.self) { _ in
			let configuration = URLSessionConfiguration.default
			configuration.httpShouldSetCookies = false
			configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
			let manager = SessionManager(configuration: configuration)
			return manager
			}.inObjectScope(.container)
		
		container.register(AuthRequestFactory.self) { resolver in
			return AuthRequestFactoryImp(
				errorHandler: resolver.resolve(AbstractErrorHandler.self)!,
				errorParser: resolver.resolve(AbstractErrorParser.self)!,
				sessionManager: resolver.resolve(SessionManager.self)!
			)
		}
		
		container.register(ChangeProfileRequestFactory.self) { resolver in
			return ChangeProfileRequestFactoryImp(
				errorHandler: resolver.resolve(AbstractErrorHandler.self)!,
				errorParser: resolver.resolve(AbstractErrorParser.self)!,
				sessionManager: resolver.resolve(SessionManager.self)!
			)
		}
		
		container.register(CatalogRequestFactory.self) { resolver in
			return CatalogRequestFactoryImp(
				errorHandler: resolver.resolve(AbstractErrorHandler.self)!,
				errorParser: resolver.resolve(AbstractErrorParser.self)!,
				sessionManager: resolver.resolve(SessionManager.self)!
			)
		}
		
		container.register(ReviewRequestFactory.self) { resolver in
			return ReviewRequestFactoryImp(
				errorHandler: resolver.resolve(AbstractErrorHandler.self)!,
				errorParser: resolver.resolve(AbstractErrorParser.self)!,
				sessionManager: resolver.resolve(SessionManager.self)!
			)
		}
		
		container.register(BasketRequestFactory.self) { resolver in
			return BasketRequestFactoryImp(
				errorHandler: resolver.resolve(AbstractErrorHandler.self)!,
				errorParser: resolver.resolve(AbstractErrorParser.self)!,
				sessionManager: resolver.resolve(SessionManager.self)!
			)
		}
	}
	
}
