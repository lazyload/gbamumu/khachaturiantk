//
//  AssemblyFactory.swift
//  GBShop
//
//  Created by Тигран on 29/10/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Swinject

let di = AssemblyFactory().makeResolver()

/// Сборщик `swinject` контейнеров.
/// Для корректной работы DI такой контейнер должен быть один на все приложение.
struct AssemblyFactory {
	
	// MARK: - Public methods
	func makeResolver() -> Resolver {
		let assembly = Assembler([NetworkAssembly(), HelpersAssembly()])
		return assembly.resolver
	}
	
}
