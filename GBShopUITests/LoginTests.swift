//
//  LoginTests.swift
//  GBShopUITests
//
//  Created by Тигран on 26/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import XCTest

class LoginTests: XCTestCase {

	var app: XCUIApplication!
	
	override func setUp() {
		continueAfterFailure = false
		app = XCUIApplication()
		setupSnapshot(app)
		app.launch()
		snapshot("Launching")
		
		super.setUp()
	}
	
	override func tearDown() {
		super.tearDown()
	}
	
	func testShouldLogin() {
		navigateToLoginVC()
		
		snapshot("Login Screen")
		
		app.textFields["GeekBrains"].tap()
		app.keys["u"].tap()
		app.keys["s"].tap()
		app.keys["e"].tap()
		app.keys["r"].tap()
		app.keys["n"].tap()
		app.keys["a"].tap()
		app.keys["m"].tap()
		app.keys["e"].tap()
		
		app.secureTextFields["••••••••"].tap()
		app.keys["more, numbers"].tap()
		app.keys["1"].tap()
		app.keys["2"].tap()
		app.keys["3"].tap()
		app.keys["4"].tap()
		app.keys["5"].tap()
		app.keys["6"].tap()
		
		app.scrollViews.firstMatch.tap()
		
		app.buttons["Вход"].tap()
		snapshot("Catalog")
		XCTAssertNotNil(app.collectionViews)
	}
	
	func testShouldFailLogin() {
		navigateToLoginVC()
		
		app.textFields["GeekBrains"].tap()
		app.keys["u"].tap()
		app.keys["s"].tap()
		app.keys["e"].tap()
		app.keys["r"].tap()
		
		app.secureTextFields["••••••••"].tap()
		app.keys["more, numbers"].tap()
		app.keys["1"].tap()
		app.keys["2"].tap()
		app.keys["3"].tap()
		
		app.scrollViews.firstMatch.tap()
		
		app.buttons["Вход"].tap()
		XCTAssertNotNil(app.alerts["Неправильная пара логин/пароль."])
	}
	
	func testShouldPrompt() {
		navigateToLoginVC()
		
		app.buttons["Забыли пароль?"].tap()
		let alert = app.alerts["""
								Имя пользователя: username
								Пароль: 123456
								"""]
		XCTAssertNotNil(alert)
		
	}
	
	private func navigateToLoginVC() {
		app.buttons["Войти"].tap()
		XCTAssertEqual(app.textFields.count, 1)
		
		app.buttons["Вход"].tap()
		XCTAssertNotNil(app.alerts)
		app.alerts.buttons["Закрыть"].tap()
	}

}
