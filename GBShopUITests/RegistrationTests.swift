//
//  RegistrationTests.swift
//  RegistrationTests
//
//  Created by Тигран on 26/11/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//
// swiftlint:disable function_body_length

import XCTest

class RegistrationTests: XCTestCase {
	
	var app: XCUIApplication!
	
	override func setUp() {
		continueAfterFailure = false
		app = XCUIApplication()
		setupSnapshot(app)
		app.launch()
		
		super.setUp()
	}
	
	override func tearDown() {
		super.tearDown()
	}
	
	func testShouldSuccess() {
		
		app.buttons["Зарегистрироваться"].tap()
		XCTAssertEqual(app.textFields.count, 2)
		
		snapshot("Registration Screen")
		
		app.buttons["Создать аккаунт"].tap()
		XCTAssertNotNil(app.alerts)
		app.alerts.buttons["Закрыть"].tap()
		
		app.textFields["GeekBrains"].tap()
		app.keys["u"].tap()
		app.keys["s"].tap()
		app.keys["e"].tap()
		app.keys["r"].tap()
		app.keys["n"].tap()
		app.keys["a"].tap()
		app.keys["m"].tap()
		app.keys["e"].tap()
		
		app.textFields["name@email.com"].tap()
		app.keys["more, numbers"].tap()
		app.keys["1"].tap()
		app.keys["2"].tap()
		app.keys["3"].tap()
		app.keys["@"].tap()
		app.keys["more, letters"].tap()
		app.keys["g"].tap()
		app.keys["m"].tap()
		app.keys["a"].tap()
		app.keys["i"].tap()
		app.keys["l"].tap()
		app.keys["more, numbers"].tap()
		app.keys["."].tap()
		app.keys["more, letters"].tap()
		app.keys["c"].tap()
		app.keys["o"].tap()
		app.keys["m"].tap()
		
		app.scrollViews.firstMatch.swipeUp()
		app.scrollViews.firstMatch.swipeUp()
		app.secureTextFields["••••••••"].tap()
		app.keys["more, numbers"].tap()
		app.keys["1"].tap()
		app.keys["2"].tap()
		app.keys["3"].tap()
		
		app.scrollViews.firstMatch.tap()
		
		app.buttons["Создать аккаунт"].tap()
		XCTAssertNotNil(app.alerts)
		app.alerts.buttons["Ок"].tap()
		
		XCTAssertNotNil(app.collectionViews)
	}
	
}
